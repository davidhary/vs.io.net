Imports System.Net.Sockets
Imports System.Net
Namespace TcpIP

    ''' <summary> A TCP/IP Session using a Tcp Client. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/31/2015 </para></remarks>
    Public Class ClientSession
        Inherits SessionBase

#Region " CONSTRUCTION "

        ''' <summary> Default constructor. </summary>
        Public Sub New()
            MyBase.new()
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <param name="bufferSize"> Size of the buffer. </param>
        Public Sub New(ByVal bufferSize As Integer)
            MyBase.new(bufferSize)
        End Sub

#Region " I Disposable Support "

        ''' <summary> Performs application-defined tasks associated with freeing, releasing, or resetting
        ''' unmanaged resources. </summary>
        ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
        ''' release only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If Not Me.Disposed Then
                    If disposing Then

                        If Me._client IsNot Nothing Then
                            If Me._client.Connected Then
                                Me._client.Client.Shutdown(SocketShutdown.Both)
                                System.Threading.Thread.Sleep(10)
                                Me._client.Close()
                            End If
                            Me._client = Nothing
                        End If
                    End If
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#End Region

#Region " TCP CLIENT "

        ''' <summary> The TCP Client for connecting to the server. </summary>
        Private _Client As TcpClient

        ''' <summary> Gets the socket. </summary>
        ''' <value> The socket. </value>
        Protected Overrides ReadOnly Property Socket As System.Net.Sockets.Socket
            Get
                Return Me._client.Client
            End Get
        End Property

#End Region

#Region " CONNECT "

        ''' <summary> Query if this object is connected. </summary>
        ''' <returns> <c>true</c> if connected; otherwise <c>false</c> </returns>
        Public Overrides Function IsConnected() As Boolean
            Return Me._client IsNot Nothing AndAlso Me._client.Connected
        End Function

        ''' <summary> Begins a connect. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="address"> The IP address. </param>
        ''' <param name="port">    The port number. </param>
        Public Overrides Sub BeginConnect(ByVal address As String, ByVal port As Integer)

            If String.IsNullOrWhiteSpace(address) Then Throw New ArgumentNullException(NameOf(address))

            ' Close the socket if it is still open
            If Me._client IsNot Nothing AndAlso Me._client.Connected Then
                Me.Disconnect()
            End If

            ' Create the client object
            ' Connect to server non-Blocking method
            Me._Client = New TcpClient(AddressFamily.InterNetwork) With {
                .ExclusiveAddressUse = False
            }
            Me._client.BeginConnect(IPAddress.Parse(address), port, New AsyncCallback(AddressOf Me.OnConnect), Me.Socket)

        End Sub

        ''' <summary> Connect. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="address"> The IP address. </param>
        ''' <param name="port">    The port number. </param>
        Public Overrides Sub Connect(ByVal address As String, ByVal port As Integer)

            If String.IsNullOrWhiteSpace(address) Then Throw New ArgumentNullException(NameOf(address))

            ' Close the socket if it is still open
            If Me._client IsNot Nothing AndAlso Me._client.Connected Then
                Me.Disconnect()
            End If

            ' Create the client object
            ' Connect to server non-Blocking method
            Me._Client = New TcpClient(AddressFamily.InterNetwork) With {
                .ExclusiveAddressUse = False
            }
            Me._client.Connect(IPAddress.Parse(address), port)
            Me.OnConnected(System.EventArgs.Empty)
        End Sub

#End Region

    End Class

End Namespace
