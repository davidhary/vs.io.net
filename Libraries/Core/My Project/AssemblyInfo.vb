﻿Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

<Assembly: AssemblyTitle("Net Core Library")> 
<Assembly: AssemblyDescription("Net Core Library")> 
<Assembly: AssemblyProduct("Net.Core")> 
#If Not NonClsCompliant Then
<Assembly: CLSCompliant(True)> 
#End If

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: System.Runtime.InteropServices.ComVisible(False)> 
