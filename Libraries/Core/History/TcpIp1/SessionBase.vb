﻿Imports System.Text
Imports System.Net.Sockets
Imports System.Net
Imports isr.Net.Core.StringEscapeSequencesExtensions
Namespace TcpIP

    ''' <summary> A TCP/IP Client base class. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/31/2015 </para></remarks>
    Public MustInherit Class SessionBase
        Implements IDisposable

#Region " CONSTRUCTION "

        ''' <summary> Default constructor. </summary>
        Protected Sub New()
            Me.New(SessionBase.DefaultBufferSize)
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <param name="bufferSize"> Size of the buffer. </param>
        Protected Sub New(ByVal bufferSize As Integer)
            MyBase.New()
            Me._AllocateReceiveBuffer(bufferSize)
        End Sub

#Region " I Disposable Support "

        Private _Disposed As Boolean ' To detect redundant calls
        Protected ReadOnly Property Disposed As Boolean
            Get
                Return Me._Disposed
            End Get
        End Property

        ''' <summary> Performs application-defined tasks associated with freeing, releasing, or resetting
        ''' unmanaged resources. </summary>
        ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
        ''' release only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Overridable Sub Dispose(disposing As Boolean)
            Try
                If Not Me._Disposed Then
                    If disposing Then

                        For Each d As [Delegate] In Me.ReceivedEvent.SafeInvocationList
                            Try
                                RemoveHandler Me.Received, CType(d, EventHandler(Of MessageEventArgs))
                            Catch ex As Exception
                                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                            End Try
                        Next
                        For Each d As [Delegate] In Me.SentEvent.SafeInvocationList
                            Try
                                RemoveHandler Me.Sent, CType(d, EventHandler(Of MessageEventArgs))
                            Catch ex As Exception
                                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                            End Try
                        Next

                        For Each d As [Delegate] In Me.ConnectionLostEvent.SafeInvocationList
                            Try
                                RemoveHandler Me.ConnectionLost, CType(d, EventHandler(Of System.EventArgs))
                            Catch ex As Exception
                                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                            End Try
                        Next

                        For Each d As [Delegate] In Me.ConnectedEvent.SafeInvocationList
                            Try
                                RemoveHandler Me.Connected, CType(d, EventHandler(Of System.EventArgs))
                            Catch ex As Exception
                                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                            End Try
                        Next

                        For Each d As [Delegate] In Me.DisconnectedEvent.SafeInvocationList
                            Try
                                RemoveHandler Me.Disconnected, CType(d, EventHandler(Of System.EventArgs))
                            Catch ex As Exception
                                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                            End Try
                        Next

                        For Each d As [Delegate] In Me.ErrorOccurredEvent.SafeInvocationList
                            Try
                                RemoveHandler Me.ErrorOccurred, CType(d, EventHandler(Of System.IO.ErrorEventArgs))
                            Catch ex As Exception
                                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                            End Try
                        Next
                    End If
                    Me._receiveBuffer = New Byte() {}
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            Finally
                Me._Disposed = True
            End Try
        End Sub

        ''' <summary> Performs application-defined tasks associated with freeing, releasing, or resetting
        ''' unmanaged resources. </summary>
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

#End Region

#End Region

#Region " SERVER "

        ''' <summary> Discover servers listening on the specified port. </summary>
        ''' <param name="port"> The port. </param>
        Public Shared Function Discover(ByVal port As Integer, ByVal timeout As TimeSpan) As String()
            Dim _L As New List(Of String)
            Dim usedIP As New HashSet(Of String)()
            Dim ipA() As IPAddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList
            For j As Integer = 0 To IpA.Length - 1
                If IpA(j).AddressFamily = AddressFamily.InterNetwork Then ' to make sure it's an IPV4
                    Dim scanIP As String = IpA(j).ToString().Substring(0, IpA(j).ToString().LastIndexOf(".")) & "."
                    If Not usedIP.Contains(scanIP) Then
                        ' UsedIP holds the first 3 parts of an IP (ex:"192.168.1." from "192.168.1.30") 
                        ' to avoid scanning the same IP addresses more than once 
                        ' like if having a wireless network IP ("192.168.1.5") and an Ethernet Network IP ("192.168.1.5"). 
                        ' So with that IP will scan once.
                        usedIP.Add(scanIP)
                        For i As Integer = 1 To 255
                            Dim ip As String = String.Format("{0}{1}", scanIP, i)
                            If ClientSession.TryPing(ip, port, timeout) Then
                                _l.Add(String.Format("{0}:{1}", ip, port))
                            End If
                            Dispatcher.CurrentDispatcher.DoEvents
                        Next
                    End If
                End If
            Next j
            Return _l.ToArray
        End Function

        ''' <summary> Attempts to ping a server address and port. </summary>
        ''' <param name="serverAddress"> The IP address. </param>
        ''' <param name="port">      The port. </param>
        ''' <param name="timeout">   The timeout. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Public Shared Function TryPing(ByVal serverAddress As String, ByVal port As Integer, ByVal timeout As TimeSpan) As Boolean
            Dim socket As Socket = Nothing
            Try
                socket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontLinger, False)
                Dim result As IAsyncResult = socket.BeginConnect(serverAddress, port, Nothing, Nothing)
                result.AsyncWaitHandle.WaitOne(CInt(timeout.TotalMilliseconds), True)
                Return socket.Connected
            Catch
                Return False
            Finally
                If Nothing IsNot socket Then
                    socket.Close()
                End If
            End Try
        End Function

        ''' <summary> Attempts to parse from the given data. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="serverAddress"> The server address. </param>
        ''' <param name="serverIP">      [in,out] The IP. </param>
        ''' <param name="port">          [in,out] The port. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
                Public Shared Function TryParse(ByVal serverAddress As String, ByRef serverIP As String, ByRef port As Integer) As Boolean
            If String.IsNullOrWhiteSpace(serverAddress) Then Throw New ArgumentNullException(NameOf(serverAddress))
            Dim address As String() = serverAddress.Split(":"c)
            serverIP = address(0)
            If address.Length > 1 Then
                If Not Integer.TryParse(address(1), port) Then
                    port = 0
                End If
            End If
            Return port > 0
        End Function

        ''' <summary> Parse port. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="serverAddress"> The server address. </param>
        ''' <returns> An Integer. </returns>
        Public Shared Function ParsePort(ByVal serverAddress As String) As Integer
            If String.IsNullOrWhiteSpace(serverAddress) Then Throw New ArgumentNullException(NameOf(serverAddress))
            Dim port As Integer = 0
            Dim address As String() = serverAddress.Split(":"c)
            If address.Length > 1 Then
                If Not Integer.TryParse(address(1), port) Then
                    port = 0
                End If
            End If
            Return port
        End Function

#End Region

#Region " SOCKET "

        ''' <summary> Query if this object is connected. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <returns> <c>true</c> if connected; otherwise <c>false</c> </returns>
        Public MustOverride Function IsConnected() As Boolean

        ''' <summary> The default buffer size. </summary>
        Private Const DefaultBufferSize As Integer = 256

        ''' <summary> Gets or sets the socket. </summary>
        ''' <value> The socket. </value>
        Protected MustOverride ReadOnly Property Socket As Socket

        ''' <summary> Buffer for received data. </summary>
        Private _ReceiveBuffer As Byte()

        ''' <summary> Allocate receive buffer. </summary>
        ''' <param name="bufferSize"> Size of the buffer. </param>
        Private Sub AllocateReceiveBuffer(ByVal bufferSize As Integer)
            Me._receiveBuffer = New Byte(bufferSize - 1) {}
        End Sub

        ''' <summary> Allocate receive buffer. </summary>
        ''' <param name="bufferSize"> Size of the buffer. </param>
        Public Sub AllocateReceiveBuffer(ByVal bufferSize As Integer)
            Me.AllocateReceiveBuffer(bufferSize)
        End Sub

        ''' <summary> Gets the last status. </summary>

        ''' <value> The last status. </value>
        Public ReadOnly Property LastStatus As SessionStatus

        ''' <summary> Gets the number of receives. </summary>
        ''' <value> The number of receives. </value>
        Public ReadOnly Property ReceiveCount As Integer
            Get
                Return Me.Socket.Available
            End Get
        End Property

#End Region

#Region " ERROR "

        Public Event ErrorOccurred As EventHandler(Of System.IO.ErrorEventArgs)

        ''' <summary> Raises the system. io. error event. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Sub OnError(ByVal e As System.IO.ErrorEventArgs)
            If Me.LastStatus >= SessionStatus.Success Then Me._LastStatus = SessionStatus.ErrorOccurred
            Dim evt As EventHandler(Of System.IO.ErrorEventArgs) = Me.ErrorOccurredEvent
            evt?.Invoke(Me, e)
        End Sub

        ''' <summary> Raises the system. io. error event. </summary>
        ''' <param name="ex"> The ex. </param>
        Protected Sub OnError(ByVal ex As Exception)
            Me.OnError(New System.IO.ErrorEventArgs(ex))
        End Sub

#End Region

#Region " CONNECT ASYNC "

        ''' <summary> Begins a connect. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="serverAddress"> The server address. </param>
        Public Sub BeginConnect(ByVal serverAddress As String)

            If String.IsNullOrWhiteSpace(serverAddress) Then Throw New ArgumentNullException(NameOf(serverAddress))
            Dim port As Integer
            Dim ip As String = String.Empty
            If SessionBase.TryParse(serverAddress, ip, port) Then
                Me.BeginConnect(ip, port)
            End If
        End Sub

        ''' <summary> Begins a connect. </summary>
        ''' <param name="address">   The IP. </param>
        ''' <param name="port"> The port. </param>
        Public MustOverride Sub BeginConnect(ByVal address As String, ByVal port As Integer)

        ''' <summary> Executes the connect action. </summary>
        ''' <param name="asyncResult"> The asynchronous result. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Sub OnConnect(ByVal asyncResult As IAsyncResult)

            If asyncResult Is Nothing Then
                Dim ex As New ArgumentNullException(NameOf(asyncResult))
                ex.Data.Add("Info", "Failed connecting")
                Me.OnError(ex)
            Else
                ' Check if we were successful
                Try
                    ' Socket was the passed in object
                    Dim sock As Socket = CType(asyncResult.AsyncState, Socket)
                    If sock.Connected Then
                        Me.OnConnected(System.EventArgs.Empty)
                    Else
                        ' Dim ex As New OperationCanceledException()
                        Dim ex As New Sockets.SocketException(SocketError.AddressNotAvailable)
                        ex.Data.Add("Info", "Unable to connect to remote machine")
                        Me.OnError(ex)
                    End If
                Catch ex As Exception
                    ex.Data.Add("Info", "Unusual error during Connect!")
                    Me.OnError(ex)
                Finally
                End Try
            End If
        End Sub

        ''' <summary> Begins a disconnect. </summary>
        Public Sub BeginDisconnect()
            ' disconnect socket if it is still open
            If Me.Socket IsNot Nothing AndAlso Me.Socket.Connected Then
                Me.Socket.BeginDisconnect(False, New AsyncCallback(AddressOf Me.OnDisconnect), Me.Socket)
                System.Threading.Thread.Sleep(10)
            End If
        End Sub

        ''' <summary> Executes the connect action. </summary>
        ''' <param name="asyncResult"> The asynchronous result. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Sub OnDisconnect(ByVal asyncResult As IAsyncResult)

            Dim e As System.IO.ErrorEventArgs = Nothing
            If asyncResult Is Nothing Then
                Dim ex As New ArgumentNullException(NameOf(asyncResult))
                ex.Data.Add("Info", "Failed disconnecting")
                e = New System.IO.ErrorEventArgs(ex)
            Else
                ' Check if we were successful
                Try
                    ' Socket was the passed in object
                    Dim sock As Socket = CType(asyncResult.AsyncState, Socket)
                    If sock.Connected Then
                        Dim ex As New System.Net.Sockets.SocketException()
                        ex.Data.Add("Info", "Unable to disconnect from the remote machine")
                        e = New System.IO.ErrorEventArgs(ex)
                    Else
                        Me.OnDisconnected(System.EventArgs.Empty)
                    End If
                Catch ex As Exception
                    ex.Data.Add("Info", "Unusual error during disconnect!")
                    e = New System.IO.ErrorEventArgs(ex)
                Finally
                End Try
            End If
            If e IsNot Nothing Then Me.OnError(e)

        End Sub

#End Region

#Region " CONNECTION EVENTS "

        Public Event Connected As EventHandler(Of System.EventArgs)

        ''' <summary> Raises the connected event. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Sub OnConnected(ByVal e As System.EventArgs)
            Me._LastStatus = SessionStatus.Success
            Dim evt As EventHandler(Of System.EventArgs) = Me.ConnectedEvent
            evt?.Invoke(Me, e)
        End Sub

        Public Event ConnectionLost As EventHandler(Of System.EventArgs)

        ''' <summary> Raises the connection lost event. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Sub OnConnectionLost(ByVal e As System.EventArgs)
            If Me.LastStatus >= SessionStatus.Success Then Me._LastStatus = SessionStatus.ConnectionLost
            Dim evt As EventHandler(Of System.EventArgs) = Me.ConnectionLostEvent
            evt?.Invoke(Me, e)
        End Sub

        Public Event Disconnected As EventHandler(Of System.EventArgs)

        ''' <summary> Raises the system. event. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Sub OnDisconnected(ByVal e As System.EventArgs)
            If Me.LastStatus >= SessionStatus.Success Then Me._LastStatus = SessionStatus.ConnectionLost
            Dim evt As EventHandler(Of System.EventArgs) = Me.DisconnectedEvent
            evt?.Invoke(Me, e)
        End Sub

#End Region

#Region " CONNECT SYNC "

        ''' <summary> Connect. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="sessionAddress"> The server address. </param>
        Public Sub Connect(ByVal sessionAddress As String)

            If String.IsNullOrWhiteSpace(sessionAddress) Then Throw New ArgumentNullException(NameOf(sessionAddress))
            Dim port As Integer
            Dim ip As String = String.Empty
            If SessionBase.TryParse(sessionAddress, ip, port) Then
                Me.Connect(ip, port)
            End If
        End Sub

        ''' <summary> Connect. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="address"> The server address. </param>
        ''' <param name="port">          The port. </param>
        Public MustOverride Sub Connect(ByVal address As String, ByVal port As Integer)

        ''' <summary> Disconnects this object. </summary>
        Public Sub Disconnect()
            If Me.Socket IsNot Nothing Then
                If Me.Socket.Connected Then
                    Me.Socket.Disconnect(False)
                    Me.OnDisconnected(System.EventArgs.Empty)
                    System.Threading.Thread.Sleep(10)
                End If
                Me.Socket.Shutdown(SocketShutdown.Both)
                System.Threading.Thread.Sleep(10)
                Me.Socket.Close()
            End If
        End Sub

#End Region

#Region " RECEIVE ASYNC WITH EVENTS "

        ''' <summary> Setup the callback for received data and loss of connection </summary>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Public Sub EnableReceiveEvents()
            Me.Socket.BeginReceive(Me._receiveBuffer, 0, Me._receiveBuffer.Length, SocketFlags.None,
                                   New AsyncCallback(AddressOf Me.OnReceived), Me.Socket)
        End Sub

        ''' <summary> Get the new data and send it out to all other connections. If not data was received
        ''' the connection has probably died. </summary>
        ''' <param name="asyncResult"> The asynchronous result. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Sub OnReceived(ByVal asyncResult As IAsyncResult)

            Dim e As System.IO.ErrorEventArgs = Nothing
            If asyncResult Is Nothing Then
                Dim ex As New ArgumentNullException(NameOf(asyncResult))
                ex.Data.Add("Info", "Failed receiving")
                e = New System.IO.ErrorEventArgs(ex)
            Else
                ' Socket was the passed in object
                Dim sock As Socket = CType(asyncResult.AsyncState, Socket)

                ' Check if we got any data
                Try
                    If sock.Connected Then
                        Dim receivedMessageLength As Integer = sock.EndReceive(asyncResult)
                        If receivedMessageLength > 0 Then

                            ' Wrote the data to the List
                            Dim receivedString As String = Encoding.ASCII.GetString(Me._receiveBuffer, 0, receivedMessageLength)

                            Me.OnReceived(receivedString)

                            ' If the connection is still usable reestablish the callback
                            sock.BeginReceive(Me._receiveBuffer, 0, Me._receiveBuffer.Length, SocketFlags.None,
                                              New AsyncCallback(AddressOf Me.OnReceived), sock)
                        Else
                            ' If no data was received then the connection is probably dead
                            ' Console.WriteLine("Client {0} disconnected", sock.RemoteEndPoint)
                            sock.Shutdown(SocketShutdown.Both)
                            System.Threading.Thread.Sleep(10)
                            sock.Close()
                            Me.OnConnectionLost(System.EventArgs.Empty)
                        End If
                    End If
                Catch ex As Exception
                    ex.Data.Add("Info", "Unusual error during receive!")
                    e = New System.IO.ErrorEventArgs(ex)
                Finally
                End Try
            End If
            If e IsNot Nothing Then Me.OnError(e)

        End Sub

        ''' <summary> Executes the received action. </summary>
        ''' <param name="value"> The value. </param>
        Protected Sub OnReceived(ByVal value As String)
            Me.OnReceived(New MessageEventArgs(value))
        End Sub

        Public Event Received As EventHandler(Of MessageEventArgs)

        ''' <summary> Executes the received action. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Sub OnReceived(ByVal e As MessageEventArgs)
            Dim evt As EventHandler(Of MessageEventArgs) = Me.ReceivedEvent
            evt?.Invoke(Me, e)
        End Sub

        ''' <summary> Reads and returns the received data. </summary>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <returns> A String. </returns>
        Public Function Read() As String
            If Me.Socket Is Nothing OrElse Not Me.Socket.Connected Then
                Throw New InvalidOperationException("Must be connected to receive a message")
            End If
            Me._LastStatus = SessionStatus.Success
            Dim receivedDataLength As Integer = Me.Socket.Receive(Me._receiveBuffer)
            Dim v As String = String.Empty
            If receivedDataLength > 0 Then
                v = Encoding.ASCII.GetString(Me._receiveBuffer, 0, receivedDataLength)
            End If
            Return v
        End Function

#End Region

#Region " RECEIVE "

        Private Property ReadLineBuilder As System.Text.StringBuilder
        Private Property Termination As Char()

        ''' <summary> Reads a line. </summary>
        ''' <param name="termination"> The termination. </param>
        ''' <param name="timeout">     The timeout. </param>
        ''' <returns> The line. </returns>
        Public Function ReadLine(ByVal termination As Char(), ByVal timeout As TimeSpan) As String
            Me.Termination = termination
            Me.ReadLineBuilder = New System.Text.StringBuilder
            Dim endTime As Date = DateTime.Now.Add(timeout)
            Me._LastStatus = SessionStatus.Active
            Me.Socket.BeginReceive(Me._receiveBuffer, 0, Me._receiveBuffer.Length, SocketFlags.None,
                                   New AsyncCallback(AddressOf Me.OnRead), Me.Socket)
            Do Until Me._LastStatus <> SessionStatus.Active
                If endTime > DateTime.Now Then Me._LastStatus = SessionStatus.TimedOut
                Dispatcher.CurrentDispatcher.DoEvents
            Loop
            Return Me.ReadLineBuilder.ToString
        End Function

        ''' <summary> Get the new data and build the line. If not data was received the connection has probably died. </summary>
        ''' <param name="asyncResult"> The asynchronous result. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Sub OnRead(ByVal asyncResult As IAsyncResult)

            If asyncResult Is Nothing Then
                Dim ex As New ArgumentNullException(NameOf(asyncResult))
                ex.Data.Add("Info", "Failed receiving")
                Me.OnError(ex)
            Else
                ' Socket was the passed in object
                Dim sock As Socket = CType(asyncResult.AsyncState, Socket)

                ' Check if we got any data
                Try
                    If sock.Connected Then
                        Dim receivedMessageLength As Integer = sock.EndReceive(asyncResult)
                        If receivedMessageLength > 0 Then

                            ' Wrote the data to the List
                            Dim receivedString As String = Encoding.ASCII.GetString(Me._receiveBuffer, 0, receivedMessageLength)

                            ' insert escape characters
                            Me.ReadLineBuilder.Append(receivedString)

                            If receivedString.EndsWith(Me.Termination) Then
                                Me._LastStatus = SessionStatus.Success
                            End If

                            If SessionStatus.Active = Me._LastStatus Then
                                sock.BeginReceive(Me._receiveBuffer, 0, Me._receiveBuffer.Length, SocketFlags.None,
                                   New AsyncCallback(AddressOf Me.OnRead), sock)
                            End If

                        Else
                            ' If no data was received then the connection is probably dead
                            ' Console.WriteLine("Client {0} disconnected", sock.RemoteEndPoint)
                            sock.Shutdown(SocketShutdown.Both)
                            System.Threading.Thread.Sleep(10)
                            sock.Close()
                            Me.OnConnectionLost(System.EventArgs.Empty)
                        End If
                    Else
                        Me.OnDisconnected(System.EventArgs.Empty)
                    End If
                Catch ex As Exception
                    ex.Data.Add("Info", "Unusual error during receive!")
                    Me.OnError(ex)
                Finally
                End Try
            End If

        End Sub

#End Region

#Region " DISCARD UNREAD DATA "

        Private _DiscardedData As System.Text.StringBuilder

        ''' <summary> Gets the information describing the discarded. </summary>
        ''' <value> Information describing the discarded. </value>
        Public ReadOnly Property DiscardedData As String
            Get
                If Me._DiscardedData Is Nothing Then
                    Return String.Empty
                Else
                    Return Me._DiscardedData.ToString
                End If
            End Get
        End Property

        ''' <summary> Reads and discards all data from the VISA session until the END indicator is read and no data are 
        '''           added to the output buffer. </summary>
        ''' <param name="pollDelay">            Time to wait between service requests. </param>
        ''' <param name="timeout">              Specifies the time to wait for message available. </param>
        Public Sub DiscardUnreadData(ByVal pollDelay As TimeSpan, ByVal timeout As TimeSpan)

            Me._LastStatus = SessionStatus.Active
            Me._DiscardedData = New System.Text.StringBuilder
            Dim availableCount As Integer? = 0
            Do

                Dim endTime As Date = DateTime.Now.Add(timeout)

                ' allow message available time to materialize
                Do Until availableCount.GetValueOrDefault(0) > 0 OrElse DateTime.Now > endTime
                    isr.Net.My.MyLibrary.DoEventsDelay(pollDelay)
                    availableCount = Me.Socket?.Available
                Loop
                availableCount = Me.Socket?.Available

                If availableCount.GetValueOrDefault(0) > 0 Then
                    Me._DiscardedData.Append(Me.Read())
                End If

            Loop While availableCount.GetValueOrDefault(0) > 0 AndAlso Me.LastStatus >= SessionStatus.Success





        End Sub

#End Region

#Region " SEND "

        Public Event Sent As EventHandler(Of MessageEventArgs)

        ''' <summary> Raises the message event. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Sub OnSent(ByVal e As MessageEventArgs)
            Dim evt As EventHandler(Of MessageEventArgs) = Me.SentEvent
            evt?.Invoke(Me, e)
        End Sub

        ''' <summary> Send the Message in the Message area. Only do this if we are connected </summary>
        Public Function Send(ByVal values As Byte()) As Integer

            Me._LastStatus = SessionStatus.Success
            Dim count As Integer = 0
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))

            ' Check we are connected
            If Me.Socket Is Nothing OrElse Not Me.Socket.Connected Then
                Throw New InvalidOperationException("Must be connected to send a message")
            End If
            count = Me.Socket.Send(values, values.Length, 0)

            Me.OnSent(New MessageEventArgs(values))

            Return count
        End Function

        ''' <summary> Send the Message in the Message area. Only do this if we are connected </summary>
        Public Function Send(ByVal value As String) As Integer

            Me._LastStatus = SessionStatus.Success
            If String.IsNullOrWhiteSpace(value) Then Return 0

            ' Convert to byte array and send.
            Return Me.Send(Encoding.ASCII.GetBytes(value.ToCharArray()))

        End Function

#End Region

    End Class

    ''' <summary> Values that represent session status. Positive values represent warnings or infomation. </summary>
    Public Enum SessionStatus
        <ComponentModel.Description("Success")> Success = 0
        <ComponentModel.Description("Timed Out")> TimedOut = -1
        <ComponentModel.Description("Error Occurred")> ErrorOccurred = -2
        <ComponentModel.Description("Disconnected")> Disconnected = -3
        <ComponentModel.Description("Connection Lost")> ConnectionLost = -4
        <ComponentModel.Description("Active")> Active = 1
    End Enum

End Namespace



#Region " UNUSED "
#If False Then
        ''' <summary> Gets the is messasge available. </summary>
        ''' <value> The is messasge available. </value>
        Public ReadOnly Property IsMessasgeAvailable As Boolean
            Get
                Return Me.Socket.Available > 0
            End Get
        End Property
#End If
#End Region