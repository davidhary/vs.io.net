﻿Imports System.Net.Sockets
Imports System.Net
Namespace TcpIP

    ''' <summary> A TCP/IP Session using a socket. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/31/2015 </para></remarks>
    Public Class SocketSession
        Inherits SessionBase
        Implements IDisposable

#Region " CONSTRUCTION "

        ''' <summary> Default constructor. </summary>
        Public Sub New()
            MyBase.new()
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <param name="bufferSize"> Size of the buffer. </param>
        Public Sub New(ByVal bufferSize As Integer)
            MyBase.new(bufferSize)
        End Sub

#Region " I Disposable Support "

        ''' <summary> Performs application-defined tasks associated with freeing, releasing, or resetting
        ''' unmanaged resources. </summary>
        ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
        ''' release only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If Not Me.Disposed Then
                    If disposing Then
                        If Me._socket IsNot Nothing Then
                            If Me._socket.Connected Then
                                Me._socket.Shutdown(SocketShutdown.Both)
                                System.Threading.Thread.Sleep(10)
                                Me._socket.Close()
                            End If
                            Me._socket.Dispose()
                            Me._socket = Nothing
                        End If
                    End If
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#End Region

#Region " SOCKET "

        ''' <summary> The socket for connecting to the server. </summary>
        Private _Socket As Socket

        ''' <summary> Gets the socket. </summary>
        ''' <value> The socket. </value>
        Protected Overrides ReadOnly Property Socket As System.Net.Sockets.Socket
            Get
                Return Me._socket
            End Get
        End Property

#End Region

#Region " CONNECT "

        ''' <summary> Query if this object is connected. </summary>
        ''' <returns> <c>true</c> if connected; otherwise <c>false</c> </returns>
        Public Overrides Function IsConnected() As Boolean
            Return Me._socket IsNot Nothing AndAlso Me._socket.Connected
        End Function

        ''' <summary> Begins a connect. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="address"> The IP address. </param>
        ''' <param name="port">    The port number. </param>
        Public Overrides Sub BeginConnect(ByVal address As String, ByVal port As Integer)

            If String.IsNullOrWhiteSpace(address) Then Throw New ArgumentNullException(NameOf(address))

            ' Close the socket if it is still open
            If Me._socket IsNot Nothing AndAlso Me._socket.Connected Then
                Me.Disconnect()
            End If

            ' Create the socket object
            Me._socket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)

            ' Define the Server address and port
            Dim serverEndPoint As New IPEndPoint(IPAddress.Parse(address), port)

            ' Connect to server non-Blocking method
            Me.Socket.Blocking = False
            Me.Socket.BeginConnect(serverEndPoint, New AsyncCallback(AddressOf Me.OnConnect), Me.Socket)

        End Sub

        ''' <summary> Connect. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="address"> The IP address. </param>
        ''' <param name="port">    The port number. </param>
        Public Overrides Sub Connect(ByVal address As String, ByVal port As Integer)

            If String.IsNullOrWhiteSpace(address) Then Throw New ArgumentNullException(NameOf(address))

            ' Close the socket if it is still open
            If Me._socket IsNot Nothing AndAlso Me._socket.Connected Then
                Me.Disconnect()
            End If

            ' Create the socket object
            Me._socket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)

            ' Define the Server address and port
            Dim serverEndPoint As New IPEndPoint(IPAddress.Parse(address), port)

            ' Connect to server non-Blocking method
            Me._socket.Blocking = False
            Me._socket.Connect(serverEndPoint)
            Me.OnConnected(System.EventArgs.Empty)

        End Sub

#End Region

    End Class

End Namespace
