﻿Namespace TcpIP

    ''' <summary> A class representing the arguments passed back to the user after a message was sent or received. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/28/2015 </para></remarks>
    Public Class MessageEventArgs
        Inherits System.EventArgs

        ''' <summary> Constructor. </summary>
        ''' <param name="value"> The value. </param>
        Public Sub New(ByVal value As String)
            MyBase.new()
            If String.IsNullOrWhiteSpace(value) Then
                Me._Data = New Byte() {}
            Else
                Me.initialize(System.Text.Encoding.ASCII.GetBytes(value.ToCharArray()))
            End If
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <param name="data"> The data. </param>
        Public Sub New(ByVal data As Byte())
            MyBase.new()
            Me.initialize(data)
        End Sub

        ''' <summary> Initializes this object. </summary>
        ''' <param name="data"> The data. </param>
        Private Sub Initialize(ByVal data As Byte())
            If data Is Nothing OrElse data.Count = 0 Then
                Me._Data = New Byte() {}
            Else
                Me._Data = New Byte(data.Count - 1) {}
                Buffer.BlockCopy(data, 0, Me._Data, 0, data.Count)
            End If
        End Sub

        Private _Data As Byte()
        ''' <summary> The data that was received. </summary>
        Public Function Data() As Byte()
            Return Me._Data
        End Function

        ''' <summary> Gets the value. </summary>
        ''' <value> The value. </value>
        Public ReadOnly Property Value As String
            Get
                If Me.Data Is Nothing OrElse Me._Data.Count = 0 Then
                    Return String.Empty
                Else
                    Return System.Text.Encoding.ASCII.GetString(Me._Data)
                End If
            End Get
        End Property
    End Class

End Namespace
