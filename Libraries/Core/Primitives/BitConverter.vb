﻿''' <summary> A static class representing a collections of bit operations. </summary>
''' <remarks> (c) 2010 Andre Trollip. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/29/2015 </para></remarks>
Public NotInheritable Class BitConverter

    ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
    Private Sub New()
    End Sub

    ''' <summary> Returns the specified integer as an array of bytes. </summary>
    ''' <param name="value"> The number to convert. </param>
    ''' <returns> An array of bytes representing the specified number. </returns>
    Public Shared Function GetBytes(ByVal value As Integer) As Byte()
        Dim toReturn As Byte() = System.BitConverter.GetBytes(value)
        CheckByteArray(toReturn)
        Return toReturn
    End Function

    ''' <summary> Returns the specified Long as an array of bytes. </summary>
    ''' <param name="value"> The number to convert. </param>
    ''' <returns> An array of bytes representing the specified number. </returns>
    Public Shared Function GetBytes(ByVal value As Long) As Byte()
        Dim toReturn As Byte() = System.BitConverter.GetBytes(value)
        CheckByteArray(toReturn)
        Return toReturn
    End Function

    ''' <summary> Reverses an array of bytes if necessary. </summary>
    ''' <param name="toCheck"> The array of bytes to reverse. </param>
    Private Shared Sub CheckByteArray(ByVal toCheck() As Byte)
        If System.BitConverter.IsLittleEndian Then
            System.Array.Reverse(toCheck)
        End If
    End Sub

    ''' <summary> Returns a 32-bit unsigned integer converted from four bytes at a specified position
    ''' in a byte array. </summary>
    ''' <param name="value">      An array of bytes. </param>
    ''' <param name="startIndex"> The starting position within value. </param>
    ''' <returns> A Integer representing the specified value. </returns>
    Public Shared Function ToInteger(ByVal value() As Byte, ByVal startIndex As Integer) As System.Int32
        Dim toConvert As Byte() = New Byte(3) {}
        System.Buffer.BlockCopy(value, startIndex, toConvert, 0, 4)

        If System.BitConverter.IsLittleEndian Then
            System.Array.Reverse(toConvert)
        End If

        Return System.BitConverter.ToInt32(toConvert, 0)
    End Function

End Class
