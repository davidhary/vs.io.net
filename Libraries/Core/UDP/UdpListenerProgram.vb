Imports System
Imports System.Net
Imports System.Net.Sockets
Imports System.Text

' use to get all the listening ports:
' https://msdn.microsoft.com/en-us/library/tst0kwb1.aspx?cs-save-lang=1&cs-lang=vb#code-snippet-1

Public Class Program
   
    Overloads Public Shared Function Main(args() As [String]) As Integer
      Dim s As New Socket(AddressFamily.InterNetwork, SocketType.Dgram,
          ProtocolType.Udp)
      Dim broadcast As IPAddress = IPAddress.Parse("192.168.1.255")
      Dim sendbuf As Byte() = Encoding.ASCII.GetBytes(args(0))
      Dim ep As New IPEndPoint(broadcast, 11000)
      s.SendTo(sendbuf, ep)
      Console.WriteLine("Message sent to the broadcast address")
   End Function 'Main
End Class 
