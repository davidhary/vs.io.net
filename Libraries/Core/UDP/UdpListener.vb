Imports System
Imports System.Net
Imports System.Net.Sockets
Imports System.Text

' use to get all the listening ports:
' https://stackoverflow.com/questions/4686083/c-sharp-sockets-how-to-get-ip-addresses-and-port-of-all-listening-sockets-c
' Or you can send out a broadcast on your subnet and wait for servers to respond. This is how some of the peer networking works in Windows for example. Note that this assumes you are the developer of the server as well and you can add in the logic necessary for the server to listen for broadcasts.
' https://msdn.microsoft.com/en-us/library/tst0kwb1.aspx?cs-save-lang=1&cs-lang=vb#code-snippet-1

Public Class UDPListener
   Private Const listenPort As Integer = 11000
   
   Private Shared Sub StartListener()
      Dim done As Boolean = False
      Dim listener As New UdpClient(listenPort)
      Dim groupEP As New IPEndPoint(IPAddress.Any, listenPort)
      Try
         While Not done
            Console.WriteLine("Waiting for broadcast")
            Dim bytes As Byte() = listener.Receive(groupEP)
            Console.WriteLine("Received broadcast from {0} :", _
                groupEP.ToString()) 
            Console.WriteLine( _
                Encoding.ASCII.GetString(bytes, 0, bytes.Length))
            Console.WriteLine()
         End While
      Catch e As Exception
         Console.WriteLine(e.ToString())
      Finally
         listener.Close()
      End Try
   End Sub 'StartListener
   
   Public Shared Function Main() As Integer
      StartListener()
      Return 0
   End Function 'Main
End Class 'UDPListener
