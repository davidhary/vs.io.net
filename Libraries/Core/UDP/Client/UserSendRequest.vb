﻿Imports System.Net

Namespace Udp

    ''' <summary> A class representing a request to send data from a user. </summary>
    ''' <remarks> (c) 2010 Andre Trollip. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/28/2015 </para></remarks>
    Friend Class UserSendRequest

        ''' <summary> Gets or sets the data. </summary>
        ''' <value> The data. </value>
        Public Property Data As Byte()

        ''' <summary> Gets or sets the size of the packet. </summary>
        ''' <value> The size of the packet. </value>
        Public Property PacketSize As Integer

        ''' <summary> Gets or sets the host. </summary>
        ''' <value> The host. </value>
        Public Property Host As IPEndPoint

        ''' <summary> Gets or sets a value indicating whether this object is reliable. </summary>
        ''' <value> <c>true</c> if reliable; otherwise <c>false</c> </value>
        Public Property Reliable As Boolean

        ''' <summary> Gets or sets the identifier. </summary>
        ''' <value> The identifier. </value>
        Public Property Id As Integer

        ''' <summary> Gets or sets a list of sends. </summary>
        ''' <value> A List of sends. </value>
        Public Property SendList As List(Of Client.Packet)

        ''' <summary> Gets or sets the maximum tries. </summary>
        ''' <value> The maximum tries. </value>
        Public Property MaxTriesCount As Integer

        ''' <summary> Gets or sets the time out. </summary>
        ''' <value> The time out. </value>
        Public Property Timeout As TimeSpan

        ''' <summary> Breaks up the message into packets. </summary>
        Public Sub PopulateSendList()

            Dim messageId() As Byte = BitConverter.GetBytes(Me.Id)

            If Me.Reliable Then
                messageId(0) = CByte(messageId(0) Or CByte(128))
            End If

            ' Dim id As Integer = BitConverter.ToInteger(messageId, 0)

            Dim packetsCount As Integer = Me.Data.Length \ Me.PacketSize
            If Me.Data.Length Mod Me.PacketSize <> 0 Then
                packetsCount += 1
            End If

            Dim counter As Integer = 1
            Dim index As Integer = 0
            Do While index < Me.Data.Length
                Dim packetLength As Integer
                If counter = packetsCount Then
                    packetLength = Me.Data.Length Mod CInt(Me.PacketSize)
                Else
                    packetLength = CInt(Me.PacketSize)
                End If

                Dim block As Byte() = New Byte(packetLength + 12 - 1) {}
                Buffer.BlockCopy(messageId, 0, block, 0, 4)
                Buffer.BlockCopy(BitConverter.GetBytes(counter), 0, block, 4, 4)
                counter += 1
                Buffer.BlockCopy(BitConverter.GetBytes(packetsCount), 0, block, 8, 4)
                Buffer.BlockCopy(Me.Data, index, block, 12, packetLength)

                index += CInt(Me.PacketSize)
                Me.SendList.Add(New Client.Packet() With {.Data = block, .SendStatus = SendStatus.None, .Tries = 0})
            Loop
        End Sub

    End Class

    ''' <summary> The information payload. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/28/2015 </para></remarks>
    Friend Class Payload

        ''' <summary> Gets or sets the entire block of data including the preamble and actual data. </summary>
        ''' <value> Information describing the received. </value>
        Public Property DataBlock As Byte()

        ''' <summary> Gets or sets a value indicating whether this object is reliable. </summary>
        ''' <value> <c>true</c> if reliable; otherwise <c>false</c> </value>
        Public Property Reliable As Boolean

        ''' <summary> Gets or sets a value indicating whether the receipt. </summary>
        ''' <value> <c>true</c> if receipt; otherwise <c>false</c> </value>
        Public Property Receipt As Boolean

        ''' <summary> Gets or sets the identifier of the message. </summary>
        ''' <value> The identifier of the message. </value>
        Public Property MessageId As Integer

        ''' <summary> Gets or sets the packet position. </summary>
        ''' <value> The packet position. </value>
        Public Property PacketPosition As Integer

        ''' <summary> Gets or sets the total number of packets. </summary>
        ''' <value> The total number of packets. </value>
        Public Property TotalPackets As Integer

        ''' <summary> Gets or sets the data. </summary>
        ''' <value> The data. </value>
        Public Property Data As Byte()

        ''' <summary> Parse a byte array of data that was received over the network. </summary>
        ''' <param name="dataToParse">    The data to parse. </param>
        ''' <param name="receivedPacket"> [in,out] Message describing the received. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Shared Function TryParse(ByVal dataToParse() As Byte,
                                        ByVal receivedPacket As Payload) As Boolean
            If dataToParse Is Nothing Then
                receivedPacket.DataBlock = Array.Empty(Of Byte)()
            Else
                receivedPacket.DataBlock = New Byte(dataToParse.Length - 1) {}
                Buffer.BlockCopy(dataToParse, 0, receivedPacket.DataBlock, 0, dataToParse.Length)
            End If
            If dataToParse Is Nothing OrElse dataToParse.Length < 12 Then
                receivedPacket.Data = Array.Empty(Of Byte)()
                receivedPacket.Reliable = False
                receivedPacket.Receipt = False
                receivedPacket.MessageId = 0
                receivedPacket.PacketPosition = 0
                receivedPacket.TotalPackets = 0
            Else
                receivedPacket.Reliable = (dataToParse(0) And 128) = 128
                receivedPacket.Receipt = (dataToParse(0) And 64) = 64
                receivedPacket.MessageId = ((BitConverter.ToInteger(dataToParse, 0) << 2) >> 2)
                receivedPacket.PacketPosition = BitConverter.ToInteger(dataToParse, 4)
                receivedPacket.TotalPackets = BitConverter.ToInteger(dataToParse, 8)
                receivedPacket.Data = New Byte(dataToParse.Length - 12 - 1) {}
                Buffer.BlockCopy(dataToParse, 12, receivedPacket.Data, 0, dataToParse.Length - 12)
            End If
            Return receivedPacket.Data.Length > 0
        End Function

    End Class
End Namespace

#Region " REPLACED "
#If False Then
        ''' <summary> Parse a byte array of data that was received over the network. </summary>
        ''' <param name="dataToParse">    The data to parse. </param>
        ''' <param name="isReliable">     [in,out] Whether the packet was sent reliably or not. </param>
        ''' <param name="isReceipt">      [in,out] Whether this packet is a delivery receipt or not. </param>
        ''' <param name="messageId">      [in,out] The ID of the message this packet is part of. </param>
        ''' <param name="packetPosition"> [in,out] The index of this packet in the message. </param>
        ''' <param name="totalPackets">   [in,out] The total number of packets of the message. </param>
        ''' <param name="data">           [in,out] The data of the packet. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Shared Function TryParse(ByVal dataToParse() As Byte,
                                         ByRef isReliable As Boolean,
                                         ByRef isReceipt As Boolean,
                                         ByRef messageId As Integer,
                                         ByRef packetPosition As Integer,
                                         ByRef totalPackets As Integer,
                                         ByRef data() As Byte) As Boolean
            If dataToParse Is Nothing OrElse dataToParse.Length < 12 Then
                isReliable = False
                isReceipt = False
                messageId = 0
                packetPosition = 0
                totalPackets = 0
                data = New Byte() {}
                Return False
            End If

            isReliable = (dataToParse(0) And 128) = 128
            isReceipt = (dataToParse(0) And 64) = 64
            messageId = ((BitConverter.ToInteger(dataToParse, 0) << 2) >> 2)
            packetPosition = BitConverter.ToInteger(dataToParse, 4)
            totalPackets = BitConverter.ToInteger(dataToParse, 8)
            data = New Byte(dataToParse.Length - 12 - 1) {}
            Buffer.BlockCopy(dataToParse, 12, data, 0, dataToParse.Length - 12)

            Return True
        End Function
#End If
#End Region