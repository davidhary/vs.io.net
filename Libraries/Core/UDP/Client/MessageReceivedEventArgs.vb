﻿Namespace Udp

    ''' <summary> A class representing the arguments passed back to the user after a message was sent. </summary>
    ''' <remarks> (c) 2010 Andre Trollip. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/28/2015 </para></remarks>
    Public Class MessageReceivedEventArgs
        Inherits System.EventArgs

        ''' <summary> The identifier of the message. </summary>
        Public Property MessageId As Integer

        ''' <summary> The data that was received. </summary>
        <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
        Public Property Data As Byte()

        ''' <summary> The host that sent the message. </summary>
        Public Property RemoteHost As System.Net.IPEndPoint

    End Class

End Namespace
