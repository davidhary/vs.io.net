Namespace Udp

    ''' <summary> A class representing a custom, synchronized dictionary. </summary>
    ''' <remarks> (c) 2010 Andre Trollip. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/28/2015 </para></remarks>
    Friend Class SynchronizedDictionary(Of T)

#Region " CONSTRUCTION "

        ''' <summary> Default constructor. </summary>
        ''' <param name="itemExpiryTimeOut"> The lifetime in seconds of items in the dictionary. </param>
        Public Sub New(ByVal itemExpiryTimeOut As Integer)
            Me._MessageSets = New Dictionary(Of Integer, T)()
            Me._ItemTimeOut = If(itemExpiryTimeOut < 0, 0, itemExpiryTimeOut)
            Me._ObjectTimes = New Queue(Of SynchronizedDictionary(Of T).TrackItem)()
        End Sub

#End Region

#Region " INTERNAL CLASSES "

        ''' <summary> An item which represents a dictionary key and the time it was added to the
        ''' dictionary. </summary>
        ''' <remarks> (c) 2010 Andre Trollip. All rights reserved.<para>
        ''' Licensed under The MIT License.</para><para>
        ''' David, 7/28/2015 </para></remarks>
        Private Class TrackItem

            ''' <summary> Default constructor. </summary>
            ''' <param name="dictionaryKey"> The key of the dictionary item. </param>
            Public Sub New(ByVal dictionaryKey As Integer)
                Me.DictionaryKey = dictionaryKey
                Me._CreatedDateTime = DateTimeOffset.Now
            End Sub

            ''' <summary> The key of this item. </summary>
            Public ReadOnly Property DictionaryKey As Integer

            Private _CreatedDateTime As DateTimeOffset = DateTimeOffset.Now
            ''' <summary> The time the item was inserted in the dictionary. </summary>
            Public ReadOnly Property CreatedDateTime As DateTimeOffset
                Get
                    Return Me._CreatedDateTime
                End Get

            End Property

        End Class

#End Region

        ''' <summary> A dictionary that stores all the reliable message that was sent. </summary>
        Private ReadOnly _MessageSets As Dictionary(Of Integer, T)

        ''' <summary> The time in seconds that items will live in the dictionary. </summary>
        Private ReadOnly _ItemTimeOut As Integer

        ''' <summary> A queue to keep a list of added items to the dictionary. </summary>
        Private ReadOnly _ObjectTimes As Queue(Of TrackItem)

        Private ReadOnly _SyncLocker As New Object
        ''' <summary> Adds a message to the dictionary. </summary>
        ''' <param name="messageId"> The key. </param>
        ''' <param name="message">   The value. </param>
        Public Sub Add(ByVal messageId As Integer, ByVal message As T)
            SyncLock Me._SyncLocker
                Me.CleanDictionary()
                Me._MessageSets.Add(messageId, message)
                Me._ObjectTimes.Enqueue(New SynchronizedDictionary(Of T).TrackItem(messageId))
            End SyncLock
        End Sub

        ''' <summary> Tries to get a value from the dictionary. </summary>
        ''' <param name="key">   The key. </param>
        ''' <param name="value"> The value. </param>
        ''' <returns> A Boolean indicating whether the retrieval was successful or not. </returns>
        Public Function TryGetValue(ByVal key As Integer, <System.Runtime.InteropServices.Out()> ByRef value As T) As Boolean
            SyncLock Me._SyncLocker
                Return Me._MessageSets.TryGetValue(key, value)
            End SyncLock
        End Function

        ''' <summary> Tries to remove a value from the dictionary. </summary>
        ''' <param name="key"> The key to remove. </param>
        ''' <returns> A Boolean indicating whether the removal was successful or not. </returns>
        Public Function Remove(ByVal key As Integer) As Boolean
            SyncLock Me._SyncLocker
                Return Me._MessageSets.Remove(key)
            End SyncLock
        End Function

        ''' <summary> Cleans u. </summary>
        Private Sub CleanDictionary()

            If Me._ItemTimeOut = 0 OrElse Me._ObjectTimes.Count = 0 Then
                Return
            End If

            Dim nextItem As TrackItem = Me._ObjectTimes.Peek()
            Do While nextItem IsNot Nothing
                If CType(DateTimeOffset.Now - nextItem.CreatedDateTime, TimeSpan).TotalSeconds > Me._ItemTimeOut Then
                    Me.Remove(nextItem.DictionaryKey)
                    Me._ObjectTimes.Dequeue()
                    If Me._ObjectTimes.Count > 0 Then
                        nextItem = Me._ObjectTimes.Peek()
                    Else

                        nextItem = Nothing
                    End If
                Else
                    Exit Do
                End If
            Loop
        End Sub

    End Class
End Namespace
