﻿Namespace Udp

    ''' <summary> A class representing the arguments passed back to the user after a message was sent. </summary>
    ''' <remarks> (c) 2010 Andre Trollip. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/28/2015 </para></remarks>
    Public Class MessageSentEventArgs
        Inherits System.EventArgs

        ''' <summary>
        ''' The identifier of the message.
        ''' </summary>
        Public Property MessageId As Integer

        ''' <summary>
        ''' The send status of the message.
        ''' </summary>
        Public Property Status As SendStatus

        ''' <summary>
        ''' If the sending failed because of an unhandled exception, this field will contain the exception.
        ''' </summary>
        Public Property Exception As Exception

    End Class

End Namespace
