﻿Imports System.ComponentModel

Namespace Udp

    ''' <summary> An enumeration of the send statuses. </summary>
    Public Enum SendStatus

        ''' <summary> The message has not been sent yet. </summary>
        <Description("Not Yet Sent")> None = 0

        ''' <summary> The send failed. </summary>
        <Description("Failed")> Failed = 1

        ''' <summary> The send was successful and the delivery has been confirmed. </summary>
        <Description("Delivered: Sent and confirmed")> Delivered = 2

        ''' <summary> The message was sent. </summary>
        <Description("Sent")> Sent = 3

    End Enum

End Namespace
