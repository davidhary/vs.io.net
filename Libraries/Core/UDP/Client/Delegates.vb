﻿Namespace Udp
#If False Then
    ''' <summary> The method definition for send completion handling. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="args">   The arguments. </param>
    Public Delegate Sub OnMessageSent(ByVal sender As Object, ByVal args As MessageSentEventArgs)

    ''' <summary> The method definition for message receipt handling. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="args">   The arguments. </param>
    Public Delegate Sub OnMessageReceived(ByVal sender As Object, ByVal args As MessageReceivedEventArgs)
#End If

End Namespace
