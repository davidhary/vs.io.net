Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading

Namespace Udp

    ''' <summary> A class representing a network client. </summary>
    ''' <remarks> http://www.codeproject.com/Articles/115304/A-Simple-UDP-Messaging-Client-Toolkit By:
    ''' Andre Trollip http://www.codeproject.com/script/Membership/View.aspx?mid=2044058. <para>
	''' (c) 2010 Andre Trollip. All rights reserved. </para><para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/28/2015 </para></remarks>
    Public Class Client
        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Initializes a new instance of the Client class with a default packet size of 1024 and
        ''' IPAddress.Any as the local end point. </summary>
        Public Sub New()
            MyBase.new()

            Me.Initialize(Nothing, 1024, _DefaultMaximumTries, _DefaultReceivedMessageTimeout)
        End Sub

        ''' <summary> Initializes a new instance of the Client class using the settings provided and with a
        ''' default packet size of 1024. </summary>
        ''' <param name="localEndPoint"> An <see cref="IPEndPoint">IP end point</see> specifying the local
        ''' IP end point to use to receive messages. </param>
        Public Sub New(ByVal localEndPoint As IPEndPoint)
            MyBase.New()
            Me.Initialize(localEndPoint, 1024, _DefaultMaximumTries, _DefaultReceivedMessageTimeout)
        End Sub

        ''' <summary> Initializes a new instance of the Client class with IPAddress.Any as the local end
        ''' point. </summary>
        ''' <param name="defaultPacketSize"> The default size in bytes of the packets that the data will
        ''' be broken up into. </param>
        Public Sub New(ByVal defaultPacketSize As Integer)
            MyBase.New()
            Me.Initialize(Nothing, defaultPacketSize, _DefaultMaximumTries, _DefaultReceivedMessageTimeout)
        End Sub

        ''' <summary> Initializes a new instance of the Client class using the settings provided. </summary>
        ''' <param name="localEndPoint">     An <see cref="IPEndPoint">IP end point</see> specifying the
        ''' local IP end point to use to receive messages. </param>
        ''' <param name="defaultPacketSize"> The default size in bytes of the packets that the data will
        ''' be broken up into. </param>
        ''' <param name="maxTries">          The maximum number of tries to to send a packet before
        ''' failing the message for reliable messages. </param>
        Public Sub New(ByVal localEndPoint As IPEndPoint, ByVal defaultPacketSize As Integer, ByVal maxTries As Byte)
            MyBase.New()
            Me.Initialize(localEndPoint, defaultPacketSize, maxTries, _DefaultReceivedMessageTimeout)
        End Sub

        ''' <summary> Initializes this instance of the Client class. </summary>
        ''' <param name="localEndPoint">          An <see cref="IPEndPoint">IP end point</see> specifying the local IP end point to use to
        ''' receive messages. </param>
        ''' <param name="defaultPacketSize">      The default size in bytes of the packets that the data
        ''' will be broken up into. </param>
        ''' <param name="maxTries">               The maximum number of tries to to send a packet before
        ''' failing the message for reliable messages. </param>
        ''' <param name="receivedMessageTimeOut"> The time in seconds incomplete received messages live. </param>
        Private Sub Initialize(ByVal localEndPoint As IPEndPoint, ByVal defaultPacketSize As Integer,
                               ByVal maxTries As Byte, ByVal receivedMessageTimeOut As Integer)
            If localEndPoint Is Nothing Then
                Me._LocalEndPoint = New IPEndPoint(IPAddress.Any, 0)
            Else
                Me._LocalEndPoint = localEndPoint
            End If

            Client.ValidatePacketSize(Of ArgumentException)(defaultPacketSize)
            Client.ValidateMaxTries(Of ArgumentException)(maxTries, "maxTries")

            Me._SentMessageSets = New SynchronizedDictionary(Of UserSendRequest)(0)
            Me._ReceivedMessageSets = New SynchronizedDictionary(Of ReceivedMessage)(receivedMessageTimeOut)

            Me._DefaultPacketSize = defaultPacketSize
            Me._MaxTryCount = maxTries

            Me._Socket = New UdpClient(Me._LocalEndPoint) With {
                .DontFragment = True,
                .EnableBroadcast = False,
                .Ttl = 128
            }

            Me._Receiver = New Thread(New ThreadStart(AddressOf Me.ReceiveWork)) With {
                .IsBackground = True,
                .Priority = ThreadPriority.AboveNormal
            }
            Me._Receiver.Start()

        End Sub

#Region "I DISPOSABLE SUPPORT"

        Private _Disposed As Boolean

        ''' <summary> Gets a value indicating whether the disposed. </summary>
        ''' <value> <c>true</c> if disposed; otherwise <c>false</c> </value>
        Protected ReadOnly Property Disposed As Boolean
            Get
                Return Me._Disposed
            End Get
        End Property

        ''' <summary> Performs application-defined tasks associated with freeing, releasing, or resetting
        ''' unmanaged resources. </summary>
        ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
        ''' release only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.Disposed Then
                    If disposing Then

                        If Me.MessageSentEvent IsNot Nothing Then

                        End If
                        For Each d As [Delegate] In Me.MessageSentEvent.GetInvocationList
                            Try
                                RemoveHandler Me.MessageSent, CType(d, EventHandler(Of MessageSentEventArgs))
                            Catch ex As Exception
                                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                            End Try
                        Next

                        If Me.MessageReceivedEvent IsNot Nothing Then
                            For Each d As [Delegate] In Me.MessageReceivedEvent.GetInvocationList
                                Try
                                    RemoveHandler Me.MessageReceived, CType(d, EventHandler(Of MessageReceivedEventArgs))
                                Catch ex As Exception
                                    Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                                End Try
                            Next
                        End If

                        If Me._Receiver IsNot Nothing AndAlso Not Me._Receiver.Join(Client._ReceiverJoinTime) Then
                            Me._Receiver.Abort()
                            ' ? Me.receiver = nothing
                        End If

                        Try
                            If Me._Socket IsNot Nothing Then
                                Me._Socket.Close()
                                Me._Socket = Nothing
                            End If
                        Catch
                        End Try
                    End If
                End If
            Finally
                Me._Disposed = True
            End Try

        End Sub

        ''' <summary> Performs application-defined tasks associated with freeing, releasing, or resetting
        ''' unmanaged resources. </summary>
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Me.Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

#End Region

#End Region

#Region " EVENT HANDLERS "

        ''' <summary> Raised when a message was sent regardless of whether it was successful or not. </summary>
        Public Event MessageSent As EventHandler(Of MessageSentEventArgs)

        ' Public Event MessageSent As OnMessageSent

        ''' <summary> Raises the <see cref="MessageSent">message sent</see>  event. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Sub OnMessageSent(ByVal e As MessageSentEventArgs)
            Dim evt As EventHandler(Of MessageSentEventArgs) = Me.MessageSentEvent
            If evt IsNot Nothing Then
                Try
                    evt.Invoke(Me, e)
                Catch ' Do not let a user exceptions abort the thread.
                End Try
            End If
        End Sub

        ''' <summary> Raises the <see cref="MessageSent">message sent</see> event if bound. </summary>
        ''' <param name="messageId"> The identifier of the message. </param>
        ''' <param name="status">    The send status of the message. </param>
        ''' <param name="ex">        If the sending failed because of an unhandled exception, this field
        ''' will contain the exception. </param>
        Private Sub OnMessageSent(ByVal messageId As Integer, ByVal status As SendStatus, ByVal ex As Exception)
            Me.OnMessageSent(New MessageSentEventArgs() With {.Exception = ex, .MessageId = messageId, .Status = status})
        End Sub

        ''' <summary> Event queue for all listeners interested in MessageReceived events. </summary>
        Public Event MessageReceived As EventHandler(Of MessageReceivedEventArgs)

        ' Public Event MessageReceived As OnMessageReceived

        ''' <summary> Raises the <see cref="MessageReceived">message received</see> event. </summary>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Sub OnMessageReceived(ByVal e As MessageReceivedEventArgs)
            Dim evt As EventHandler(Of MessageReceivedEventArgs) = Me.MessageReceivedEvent
            Try
                evt?.Invoke(Me, e)
            Catch ' Do not let a user exceptions abort the thread.
            End Try

        End Sub

        ''' <summary> Raises the <see cref="MessageReceived">message received</see> event if bound. </summary>
        ''' <param name="messageId">  The identifier of the message. </param>
        ''' <param name="remoteHost"> The host that sent the message. </param>
        ''' <param name="data">       The data that was sent. </param>
        Private Sub OnMessageReceived(ByVal messageId As Integer, ByVal remoteHost As IPEndPoint, ByVal data() As Byte)
            Me.OnMessageReceived(New MessageReceivedEventArgs() With {.MessageId = messageId, .RemoteHost = remoteHost, .Data = data})
        End Sub

#End Region

#Region " CLASSES "

        ''' <summary> A class representing an individual packet. </summary>
        ''' <remarks> (c) 2010 Andre Trollip. All rights reserved.<para>
        ''' Licensed under The MIT License.</para><para>
        ''' David, 7/28/2015 </para></remarks>
        Friend Class Packet

            ''' <summary> The data. </summary>
            Public Property Data As Byte()

            ''' <summary> The sent time stamp. </summary>
            Public Property SentTimeStamp As DateTimeOffset

            ''' <summary> The send status. </summary>
            Public Property SendStatus As SendStatus

            ''' <summary> The tries. </summary>
            Public Property Tries As Integer

        End Class

        ''' <summary> A class representing a received message. </summary>
        ''' <remarks> (c) 2010 Andre Trollip. All rights reserved.<para>
        ''' Licensed under The MIT License.</para><para>
        ''' David, 7/28/2015 </para></remarks>
        Friend Class ReceivedMessage

            ''' <summary> The identifier. </summary>
            <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
            Public Property ID As Integer

            ''' <summary> The data. </summary>
            Public Property Data As Byte()()

            ''' <summary> The host. </summary>
            <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
            Public Property Host As IPEndPoint

            ''' <summary> Number of outstanding packets. </summary>
            Public Property OutstandingPacketCount As Integer

        End Class

#End Region

#Region " CONSTANTS "

        ''' <summary> The time out value for joining the receiver thread when disposing. </summary>
        Private Const _ReceiverJoinTime As Integer = 10000

        ''' <summary> The minimum size in bytes of a network packet. </summary>
        Private Const _MinimumPacketSize As Integer = 64

        ''' <summary> The maximum size in bytes of a network packet. </summary>
        Private Const _MaximumPacketSize As Integer = 1048576 ' 1 MB

        ''' <summary> The maximum number of tries the user can set. </summary>
        Private Const _MaximumNumberOfTries As Byte = 255

        ''' <summary>
        ''' The minimum number of tries the user can set.
        ''' </summary>
        Private Const _MinimumNumberOfTries As Byte = 1

        ''' <summary> The default number of maximum tries a user can set. </summary>
        Private Const _DefaultMaximumTries As Byte = 3

        ''' <summary> The minimum number of seconds to wait before assuming a packet was lost. </summary>
        Private Const _MinimumSendTimeout As Byte = 3

        ''' <summary> The default time in seconds for incomplete received messages to live. </summary>
        Private Const _DefaultReceivedMessageTimeout As Integer = 86400

#End Region

#Region " FIELDS "

        ''' <summary> The Udp Client used to send and receive data with. </summary>
        Private _Socket As UdpClient

        ''' <summary> The next message ID to use. </summary>
        Private Shared _CurrentMessageId As Integer

        ''' <summary> A lock for incrementing the current message ID. </summary>
        Private Shared ReadOnly CurrentMessageIdLock As New Object()

        ''' <summary> A dictionary that stores all the reliable message that was sent. </summary>
        Private _SentMessageSets As SynchronizedDictionary(Of UserSendRequest)

        ''' <summary> A dictionary that stores all the reliable message that was sent. </summary>
        Private _ReceivedMessageSets As SynchronizedDictionary(Of ReceivedMessage)

        ''' <summary> The maximum number of tries to send a packet. </summary>
        Private _MaxTryCount As Byte

        ''' <summary> This thread is dedicated to receiving packets. </summary>
        Private _Receiver As Thread

#End Region

#Region " LOCAL END POINT "

        ''' <summary> An IP End Point specifying the local IP end point to use to receive messages. </summary>
        Private _LocalEndPoint As IPEndPoint = Nothing

        ''' <summary> Gets or sets the IP End Point to use for receiving data. </summary>
        ''' <value> The local end point. </value>
        Public ReadOnly Property LocalEndPoint() As IPEndPoint
            Get
                Me.CheckInstanceValidity()
                Return Me._LocalEndPoint
            End Get
        End Property

        ''' <summary>
        ''' The default size in bytes of the packets that the data will be broken up into.
        ''' </summary>
        Private _DefaultPacketSize As Integer

        ''' <summary> Gets or sets the default packet size. </summary>
        ''' <value> The default packet size. </value>
        Public Property DefaultPacketSize() As Integer
            Get
                Me.CheckInstanceValidity()
                Return Me._DefaultPacketSize
            End Get
            Set(ByVal value As Integer)
                Me.CheckInstanceValidity()
                Client.ValidatePacketSize(Of ArgumentException)(value)
                Me._DefaultPacketSize = value
            End Set
        End Property

#End Region

#Region " QUEUE MESSAGE "

        ''' <summary> Sends data to a host end point. </summary>
        ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
        ''' <param name="data">       An array of bytes representing the data to send. </param>
        ''' <param name="packetSize"> The size in bytes of the packets that the data will be broken up
        ''' into. </param>
        ''' <param name="host">       The IP information of the remote host. </param>
        ''' <param name="reliable">   A Boolean indicating whether the sending should be done reliably.
        ''' That is, whether this sender will request and wait for receipts or not. </param>
        ''' <param name="timeout">    The time to wait before it is assumed that a packet has been lost. </param>
        ''' <returns> A System.Guid representing a unique ID for this message. </returns>
        Public Function QueueMessage(ByVal data() As Byte, ByVal packetSize As Integer,
                                     ByVal host As IPEndPoint, ByVal reliable As Boolean,
                                     ByVal timeout As TimeSpan) As Integer
            Me.CheckInstanceValidity()

            Client.ValidateData(Of ArgumentNullException)(data, "data")
            Client.ValidatePacketSize(Of ArgumentException)(packetSize)
            Client.ValidateHost(Of ArgumentNullException)(host, "host")
            Client.ValidateTimeOut(Of ArgumentException)(timeout, "timeOut")

            Dim toSend As New UserSendRequest With {.Data = data,
                                                    .PacketSize = packetSize,
                                                    .Host = host,
                                                    .Reliable = reliable,
                                                    .Id = GetNewMessageID(),
                                                    .MaxTriesCount = Me._MaxTryCount,
                                                    .SendList = New List(Of Packet)(),
                                                    .Timeout = timeout}

            If Not ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf Me.SendWork), toSend) Then
                Throw New NotSupportedException("Queuing of work is not supported on the Thread Pool for this operating system.")
            End If

            Return toSend.Id
        End Function

        ''' <summary> Sends data to a host end point. </summary>
        ''' <param name="data">       An array of bytes representing the data to send. </param>
        ''' <param name="packetSize"> The size in bytes of the packets that the data will be broken up
        ''' into. </param>
        ''' <param name="host">       The IP information of the remote host. </param>
        ''' <param name="timeout">    The time to wait before it is assumed that a packet has been lost. </param>
        ''' <param name="reliable">   A Boolean indicating whether the sending should be done reliably.
        ''' That is, whether this sender will request and wait for receipts or not. </param>
        ''' <returns> A System.Guid representing a unique ID for this message. </returns>
        Public Function QueueMessage(ByVal data() As Byte, ByVal packetSize As Integer, ByVal host As IPEndPoint,
                                     ByVal timeout As TimeSpan, ByVal reliable As Boolean) As Integer
            Return Me.QueueMessage(data, packetSize, host, reliable, timeout)
        End Function

        ''' <summary> Sends data to a host end point. </summary>
        ''' <param name="data">       An array of bytes representing the data to send. </param>
        ''' <param name="packetSize"> The size in bytes of the packets that the data will be broken up
        ''' into. </param>
        ''' <param name="address">    The IP address of the remote host. </param>
        ''' <param name="port">       The IP port of the remote host. </param>
        ''' <param name="reliable">   A Boolean indicating whether the sending should be done reliably.
        ''' That is, whether this sender will request and wait for receipts or not. </param>
        ''' <param name="timeout">    The time to wait before it is assumed that a packet has
        ''' been lost. </param>
        ''' <returns> A System.Guid representing a unique ID for this message. </returns>
        Public Function QueueMessage(ByVal data() As Byte, ByVal packetSize As Integer, ByVal address As IPAddress,
                                     ByVal port As Integer, ByVal reliable As Boolean, ByVal timeout As TimeSpan) As Integer
            Return Me.QueueMessage(data, packetSize, New IPEndPoint(address, port), reliable, timeout)
        End Function

        ''' <summary> Sends data to a host end point asynchronously and non-reliably. </summary>
        ''' <param name="data">       An array of bytes representing the data to send. </param>
        ''' <param name="packetSize"> The size in bytes of the packets that the data will be broken up
        ''' into. </param>
        ''' <param name="host">       The IP information of the remote host. </param>
        ''' <param name="timeout">    The time to wait before it is assumed that a packet has
        ''' been lost. </param>
        ''' <returns> A System.Guid representing a unique ID for this message. </returns>
        Public Function QueueMessage(ByVal data() As Byte, ByVal packetSize As Integer,
                                     ByVal host As IPEndPoint, ByVal timeout As TimeSpan) As Integer
            Return Me.QueueMessage(data, packetSize, host, False, timeout)
        End Function

        ''' <summary> Sends data to a host end point asynchronously and non-reliably. </summary>
        ''' <param name="data">       An array of bytes representing the data to send. </param>
        ''' <param name="packetSize"> The size in bytes of the packets that the data will be broken up
        ''' into. </param>
        ''' <param name="address">    The IP address of the remote host. </param>
        ''' <param name="port">       The IP port of the remote host. </param>
        ''' <param name="timeout">    The time to wait before it is assumed that a packet has been lost. </param>
        ''' <returns> A System.Guid representing a unique ID for this message. </returns>
        Public Function QueueMessage(ByVal data() As Byte, ByVal packetSize As Integer, ByVal address As IPAddress,
                                     ByVal port As Integer, ByVal timeout As TimeSpan) As Integer
            Return Me.QueueMessage(data, packetSize, New IPEndPoint(address, port), False, timeout)
        End Function

        ''' <summary> Sends data to a host end point asynchronously, non-reliably and with a packet size
        ''' specified by the property PacketSize. </summary>
        ''' <param name="data">    An array of bytes representing the data to send. </param>
        ''' <param name="host">    The IP information of the remote host. </param>
        ''' <param name="timeout"> The time to wait before it is assumed that a packet has been lost. </param>
        ''' <returns> A System.Guid representing a unique ID for this message. </returns>
        Public Function QueueMessage(ByVal data() As Byte, ByVal host As IPEndPoint, ByVal timeout As TimeSpan) As Integer
            Return Me.QueueMessage(data, Me._DefaultPacketSize, host, False, timeout)
        End Function

        ''' <summary> Sends data to a host end point asynchronously, non-reliably and with a packet size
        ''' specified by the property PacketSize. </summary>
        ''' <param name="data">    An array of bytes representing the data to send. </param>
        ''' <param name="address"> The IP address of the remote host. </param>
        ''' <param name="port">    The IP port of the remote host. </param>
        ''' <param name="timeout"> The time to wait before it is assumed that a packet has been lost. </param>
        ''' <returns> A System.Guid representing a unique ID for this message. </returns>
        Public Function QueueMessage(ByVal data() As Byte, ByVal address As IPAddress,
                                     ByVal port As Integer, ByVal timeout As TimeSpan) As Integer
            Return Me.QueueMessage(data, Me._DefaultPacketSize, New IPEndPoint(address, port), False, timeout)
        End Function

#End Region

#Region " Check Instance Validity "

        ''' <summary> Checks if this instance is still valid. </summary>
        ''' <exception cref="ObjectDisposedException"> Thrown when a supplied object has been disposed. </exception>
        Private Sub CheckInstanceValidity()
            If Me.Disposed Then Throw New ObjectDisposedException(Me.GetType().FullName)
        End Sub

#End Region

#Region " Validate "

        ''' <summary> Validates the packet size. </summary>
        ''' <param name="value"> The value to validate. </param>
        Private Shared Sub ValidatePacketSize(Of T)(ByVal value As Integer)
            If value < Client._MinimumPacketSize OrElse value > Client._MaximumPacketSize Then
                Dim exception As Exception = CType(Activator.CreateInstance(GetType(T),
                                                               String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                            "The specified value of packet size must be greater than or equal to {0} bytes and less than or equal to {1}  bytes.", _MinimumPacketSize, _MaximumPacketSize)),
                                                                     Exception)
                Throw exception
            End If
        End Sub

        ''' <summary> Validates the maximum number of tries. </summary>
        ''' <param name="value"> The value to validate. </param>
        ''' <param name="args">  The arguments to pass to the exception constructor. </param>
        Private Shared Sub ValidateMaxTries(Of T)(ByVal value As Byte, ByVal ParamArray args() As String)
            If value < _MinimumNumberOfTries OrElse value > _MaximumNumberOfTries Then
                Dim exception As Exception = CType(Activator.CreateInstance(GetType(T), args,
                                                             String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                           "The specified value must be greater than or equal to {0} and less than or equal to {1}.",
                                                                           Client._MinimumNumberOfTries, Client._MaximumNumberOfTries)),
                                                                   Exception)
                Throw exception
            End If
        End Sub

        ''' <summary> Validates the time out of sends. </summary>
        ''' <param name="value"> The value to validate. </param>
        ''' <param name="args">  The arguments to pass to the exception constructor. </param>
        Private Shared Sub ValidateTimeOut(Of T)(ByVal value As TimeSpan, ByVal ParamArray args() As String)
            If value.TotalSeconds < Client._MinimumSendTimeout Then
                Dim exception As Exception = CType(Activator.CreateInstance(GetType(T), args,
                                                                            String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                            "The specified timeout {0} must be greater than or equal to {1} seconds.",
                                                                            value.TotalSeconds, Client._MinimumSendTimeout)),
                                                                    Exception)
                Throw exception
            End If
        End Sub

        ''' <summary> Validates the host. </summary>
        ''' <param name="value"> The value to validate. </param>
        ''' <param name="args">  The arguments to pass to the exception constructor. </param>
        Private Shared Sub ValidateHost(Of T)(ByVal value As IPEndPoint, ByVal ParamArray args() As String)
            If value Is Nothing Then
                Dim exception As Exception = CType(Activator.CreateInstance(GetType(T), args,
                                                                            "The specified value may not be null."),
                                                                        Exception)
                Throw exception
            End If
        End Sub

        ''' <summary> Validates the data. </summary>
        ''' <param name="value"> The value to validate. </param>
        ''' <param name="args">  The arguments to pass to the exception constructor. </param>
        Private Shared Sub ValidateData(Of T)(ByVal value() As Byte, ByVal ParamArray args() As String)
            If value Is Nothing Then
                Dim exception As Exception = CType(Activator.CreateInstance(GetType(T), args,
                                                                            "The specified value may not be null."),
                                                                        Exception)
                Throw exception
            End If
        End Sub

#End Region

#Region " SEND "

        ''' <summary> Sends a user send request. </summary>
        ''' <param name="stateInfo"> The message to send. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub SendWork(ByVal stateInfo As Object)
            Dim toSend As UserSendRequest = CType(stateInfo, UserSendRequest)
            Try
                toSend.PopulateSendList()
                Me.AddToMessageSets(toSend)
                Me.SendAllPackets(toSend)
            Catch ex As Exception
                Client.RemoveFromMessageSets(Of UserSendRequest)(toSend.Id, Me._SentMessageSets)
                Me.OnMessageSent(toSend.Id, SendStatus.Failed, ex)
            End Try
        End Sub

        ''' <summary> Send the packets in the message. </summary>
        ''' <param name="toSend"> The message to send. </param>
        Private Sub SendAllPackets(ByVal toSend As UserSendRequest)
            Do
                Dim allDelivered As Boolean = True
                For i As Integer = 0 To toSend.SendList.Count - 1
                    Dim packet As Client.Packet = toSend.SendList(i)

                    Select Case packet.SendStatus
                        Case SendStatus.None
                            allDelivered = False
                            packet.SendStatus = SendStatus.Sent
#If DEBUG Then
                            Thread.Sleep(10)
#End If
                            packet.SentTimeStamp = DateTimeOffset.Now
                            packet.Tries += 1
                            Me._Socket.Send(packet.Data, packet.Data.Length, toSend.Host)

                        Case SendStatus.Failed
                            Client.RemoveFromMessageSets(Of UserSendRequest)(toSend.Id, Me._SentMessageSets)

                            Me.OnMessageSent(toSend.Id, SendStatus.Failed, Nothing)
                            Return

                        Case SendStatus.Sent
                            allDelivered = False
                            If DateTimeOffset.Now.Subtract(packet.SentTimeStamp) > toSend.Timeout Then
                                If packet.Tries >= toSend.MaxTriesCount Then
                                    Client.RemoveFromMessageSets(Of UserSendRequest)(toSend.Id, Me._SentMessageSets)
                                    Me.OnMessageSent(toSend.Id, SendStatus.Failed, Nothing)
                                    Return
                                Else
#If DEBUG Then
                                    Thread.Sleep(10)
#End If
                                    packet.SentTimeStamp = DateTimeOffset.Now
                                    packet.Tries += 1
                                    Me._Socket.Send(packet.Data, packet.Data.Length, toSend.Host)
                                End If
                            End If

                    End Select

                Next i

                If allDelivered AndAlso toSend.Reliable Then
                    Client.RemoveFromMessageSets(Of UserSendRequest)(toSend.Id, Me._SentMessageSets)
                    Me.OnMessageSent(toSend.Id, SendStatus.Delivered, Nothing)
                    Return
                ElseIf Not toSend.Reliable Then
                    Me.OnMessageSent(toSend.Id, SendStatus.Sent, Nothing)
                End If
            Loop While toSend.Reliable
        End Sub

        ''' <summary> Adds a message to the message set dictionary. </summary>
        ''' <param name="messageId">  The identifier of the message to remove. </param>
        ''' <param name="messageSet"> Set the message belongs to. </param>
        Private Shared Sub RemoveFromMessageSets(Of T)(ByVal messageId As Integer, ByVal messageSet As SynchronizedDictionary(Of T))
            Dim message As T

            If messageSet.TryGetValue(messageId, message) Then
                messageSet.Remove(messageId)
            End If
        End Sub

        ''' <summary> Adds a message to the message set dictionary. </summary>
        ''' <param name="toSend"> . </param>
        Private Sub AddToMessageSets(ByVal toSend As UserSendRequest)
            If toSend.Reliable Then
                Me._SentMessageSets.Add(toSend.Id, toSend)
            End If
        End Sub

        ''' <summary> Increments the current message ID and returns the new ID. </summary>
        ''' <returns> The new message identifier. </returns>
        Friend Shared Function GetNewMessageID() As Integer
            SyncLock CurrentMessageIdLock
                _CurrentMessageId += 1
                If _CurrentMessageId = (Integer.MaxValue << 2) >> 2 Then
                    _CurrentMessageId = 1
                End If

                Return _CurrentMessageId
            End SyncLock
        End Function

        ''' <summary> Gets or sets the maximum number of tries for each send. </summary>
        ''' <value> The maximum tries. </value>
        Public Property MaximumTries() As Byte
            Get
                Me.CheckInstanceValidity()
                Return Me._MaxTryCount
            End Get
            Set(ByVal value As Byte)
                Me.CheckInstanceValidity()
                Client.ValidateMaxTries(Of ArgumentException)(value, "MaximumTries")
                Me._MaxTryCount = value
            End Set
        End Property

#End Region

#Region " RECEIVE "

        ''' <summary> The receiver thread delegate. </summary>
        Private Sub ReceiveWork()
            Try
                Do While Not Me.Disposed
                    Me.Receive()
                Loop
            Catch ex As ThreadAbortException
                Thread.ResetAbort()
            End Try
        End Sub

        ''' <summary> Does a receive operation and takes appropriate action. </summary>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub Receive()
            Try
                Dim remoteHost As IPEndPoint = Nothing
                Dim receivedData As Byte() = Me._Socket.Receive(remoteHost)
                Dim received As New Payload
                If Payload.TryParse(receivedData, received) Then
                    'received.Data = New Byte(receivedData.Length - 12 - 1) {}
                    'Buffer.BlockCopy(receivedData, 12, received.Data, 0, receivedData.Length - 12)
                    If Not received.Receipt Then
                        If received.Reliable Then
                            Dim receipt As Byte() = New Byte(11) {}
                            Dim mesID As Byte() = BitConverter.GetBytes(received.MessageId)
                            mesID(0) = CByte(mesID(0) Or CByte(64))
                            Buffer.BlockCopy(mesID, 0, receipt, 0, 4)
                            Buffer.BlockCopy(BitConverter.GetBytes(received.PacketPosition), 0, receipt, 4, 4)
                            Buffer.BlockCopy(BitConverter.GetBytes(received.TotalPackets), 0, receipt, 8, 4)
                            Me._Socket.Send(receipt, receipt.Length, remoteHost)
                        End If
#If DEBUG Then
                        System.Diagnostics.Debug.WriteLine(received.PacketPosition)
#End If

                        Me.AddReceivedPacket(received.MessageId, received.Data, received.PacketPosition, received.TotalPackets, remoteHost)
                    Else
                        Dim message As New UserSendRequest
                        If Me._SentMessageSets.TryGetValue(received.MessageId, message) Then
                            Dim packets As List(Of Client.Packet) = message.SendList
                            packets(CInt(received.PacketPosition) - 1).SendStatus = SendStatus.Delivered
                        End If
                    End If
                End If
            Catch ex As ThreadAbortException
                Throw
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        End Sub

        ''' <summary> Adds the received packet to the message. If a message does not exist, it gets
        ''' created. If all packets for the message have been received, the message get handed to the
        ''' user. </summary>
        ''' <param name="messageId">       The identifier of the message to remove. </param>
        ''' <param name="data">            The data for the packet. </param>
        ''' <param name="packetPosition">  The packet position. </param>
        ''' <param name="numberOfPackets"> Number of packets. </param>
        ''' <param name="remoteHost">      The host that sent the message. </param>
        Private Sub AddReceivedPacket(ByVal messageId As Integer, ByVal data() As Byte, ByVal packetPosition As Integer,
                                      ByVal numberOfPackets As Integer, ByVal remoteHost As IPEndPoint)
            Dim message As New ReceivedMessage
            If Not Me._ReceivedMessageSets.TryGetValue(messageId, message) Then
                message = New ReceivedMessage() With {.ID = messageId, .Data = New Byte(numberOfPackets - 1)() {},
                                                      .Host = remoteHost, .OutstandingPacketCount = numberOfPackets}
                Me._ReceivedMessageSets.Add(messageId, message)
            End If

            If message.Data(CInt(packetPosition) - 1) Is Nothing Then
                message.OutstandingPacketCount -= 1
                message.Data(CInt(packetPosition) - 1) = data
            End If

            If message.OutstandingPacketCount > 0 Then
                Return
            End If

            Me._ReceivedMessageSets.Remove(messageId)

            Dim totalBuffer As Byte() = Array.Empty(Of Byte)
            For i As Integer = 0 To message.Data.GetUpperBound(0)
                Dim tempBuffer As Byte() = New Byte(totalBuffer.Length - 1) {}
                Buffer.BlockCopy(totalBuffer, 0, tempBuffer, 0, tempBuffer.Length)
                totalBuffer = New Byte(totalBuffer.Length + message.Data(i).Length - 1) {}
                Buffer.BlockCopy(tempBuffer, 0, totalBuffer, 0, tempBuffer.Length)
                Buffer.BlockCopy(message.Data(i), 0, totalBuffer, tempBuffer.Length, message.Data(i).Length)
            Next i

            Me.OnMessageReceived(messageId, remoteHost, totalBuffer)
        End Sub

#End Region

    End Class
End Namespace

#Region " REPLACED "
#If False Then
        ''' <summary> Does a receive operation and takes appropriate action. </summary>
        Private Sub Receive()
            Try
                Dim remoteHost As IPEndPoint = Nothing
                Dim receivedData As Byte() = socket.Receive(remoteHost)

                Dim isReliable As Boolean
                Dim isReceipt As Boolean
                Dim messageId As Integer
                Dim packetPosition As Integer
                Dim totalPackets As Integer
                Dim data() As Byte = New Byte() {}
                Dim received As New ReceivePacket
                If Not UserSendRequest.TryParse(receivedData, isReliable, isReceipt, messageId, packetPosition, totalPackets, data) Then
                    Return
                End If

                data = New Byte(receivedData.Length - 12 - 1) {}
                Buffer.BlockCopy(receivedData, 12, data, 0, receivedData.Length - 12)

                If Not isReceipt Then
                    If isReliable Then
                        Dim receipt As Byte() = New Byte(11) {}
                        Dim mesID As Byte() = BitConverter.GetBytes(messageId)
                        mesID(0) = CByte(mesID(0) Or CByte(64))
                        Buffer.BlockCopy(mesID, 0, receipt, 0, 4)
                        Buffer.BlockCopy(BitConverter.GetBytes(packetPosition), 0, receipt, 4, 4)
                        Buffer.BlockCopy(BitConverter.GetBytes(totalPackets), 0, receipt, 8, 4)
                        socket.Send(receipt, receipt.Length, remoteHost)
                    End If
#If DEBUG Then
                    System.Diagnostics.Debug.WriteLine(packetPosition)
#End If

                    Me.AddReceivedPacket(messageId, data, packetPosition, totalPackets, remoteHost)
                Else
                    Dim message As New UserSendRequest
                    If sentMessageSets.TryGetValue(messageId, message) Then
                        Dim packets As List(Of Client.Packet) = message.SendList
                        packets(CInt(packetPosition) - 1).SendStatus = SendStatus.Delivered
                    End If
                End If
            Catch
            End Try
        End Sub


#End If
#End Region
