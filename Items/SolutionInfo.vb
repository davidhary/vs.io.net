'---------------------------------------------------------------------------------------------------
' file:		.\My\SolutionInfo.vb
'
' summary:	Solution information class
'---------------------------------------------------------------------------------------------------
Imports System.Reflection
Imports System.Resources

<Assembly: AssemblyCompany("Integrated Scientific Resources")>
<Assembly: AssemblyCopyright("(c) 2015 Scientific Resources, Inc. All rights reserved.")>
<Assembly: AssemblyTrademark("Licensed under The MIT License.")>
<Assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)>
<Assembly: AssemblyCulture("")>

