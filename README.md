# Net Project

A library of support and extension classes for HTTP and UDP based communication.

## Getting Started

Clone the project along with its requisite projects to their respective relative path. All projects are installed relative to a common path.

### Prerequisites

The following projects are also required:
* [Core](https://www.bitbucket.org/davidhary/vs.core) - Core Project
* [Share](https://www.bitbucket.org/davidhary/vs.share) - Share project
* [Share](https://www.bitbucket.org/davidhary/vs.IOnet) - Net Project

```
git clone git@bitbucket.org:davidhary/vs.share.git
git clone git@bitbucket.org:davidhary/vs.core.git
git clone git@bitbucket.org:davidhary/vs.IOnet.git
```

### Installing

Install the projects into the following folders:

#### Projects relative path
```
.\Libraries\VS\Share
.\Libraries\VS\Core\Core
.\Libraries\VS\IO\Net
```
#### Global Configution Files
ISR libraries use global editor configuration and test run settings. 

Restoring Editor Configuration assuming c:\My is the root folder of the .NET solutions):
```
Copy C:\My\Libraries\VS\Share\my.editorconfig c:\My\.editorconfig
```

Restoring Run Settings assuming c:\user\<me> is the root user folder:
```
Copy C:\My\Libraries\VS\Share\my.runsettings c:\user\<me>\.runsettings
```

## Testing

The project includes a few unit test classes. Test applications are under the *Apps* solution folder. 

## Deployment

Deployment projects have not been created for this project.

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **David Hary** - *Initial Workarounds* - [ATE Coder](https://www.IntegratedScientificResources.com)

## License

This project is licensed under the [MIT License](https://www.bitbucket.org/davidhary/vs.net/src/master/LICENSE.md)

## Acknowledgments

* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com)
* [Simple UDP](https://www.codeproject.com/Articles/115304/A-Simple-UDP-Messaging-Client-Toolkit) -- Simple UDP
* [TCP.IP CLient Server](https://www.codeproject.com/Articles/1608/Asynchronous-socket-communication) -- TCP/IP Client Server

\pard\cbpat5\nowidctlpar\kerning0\b\f3\par
}

## Revision Changes

* Version 1.0.5686	07/28/13	Created.
