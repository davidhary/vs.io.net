Imports Microsoft.VisualBasic
Imports System
Imports System.Threading ' Sleeping
Imports System.Net ' Used to local machine info
Imports System.Net.Sockets ' Socket namespace
Imports System.Collections ' Access to the Array list

Namespace ChatServer
	''' <summary>
	''' Main class from which all objects are created
	''' </summary>
	Friend Class AppMain
        ' Attributes
        Private ReadOnly _Clients As New ArrayList() ' List of Client Connections

        ''' <summary>
        ''' Application starts here. Create an instance of this class and use it
        ''' as the main object.
        ''' </summary>
        ''' <param name="args"></param>
        <Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
        Public Shared Sub Main(ByVal args() As String)
            Dim app As New AppMain()
            ' Welcome and Start listening
            Console.WriteLine("*** Chat Server Started {0} *** ", DateTimeOffset.Now.ToString("G"))


            '			
            '			//
            '			// Method 1
            '			//
            '			Socket client;
            '			const int nPortListen = 399;
            '			try
            '			{
            '				TcpListener listener = new TcpListener( nPortListen );
            '				Console.WriteLine( "Listening as {0}", listener.LocalEndpoint );
            '				listener.Start();
            '				do
            '				{
            '					byte [] m_byBuff = new byte[127];
            '					if( listener.Pending() )
            '					{
            '						client = listener.AcceptSocket();
            '						// Get current date and time.
            '						DateTimeOffset now = DateTimeOffset.Now;
            '						String strDateLine = "Welcome " + now.ToString("G") + "\n\r";
            '
            '						// Convert to byte array and send.
            '						Byte[] byteDateLine = System.Text.Encoding.ASCII.GetBytes( strDateLine.ToCharArray() );
            '						client.Send( byteDateLine, byteDateLine.Length, 0 );
            '					}
            '					else
            '					{
            '						Thread.Sleep( 100 );
            '					}
            '				} while( true );	// Don't use this. 
            '
            '				//Console.WriteLine ("OK that does it! Screw you guys I'm going home..." );
            '				//listener.Stop();
            '			}
            '			catch( Exception ex )
            '			{
            '				Console.WriteLine ( ex.Message );
            '			}
            '			


            '
            ' Method 2 
            '
            Const nPortListen As Integer = 399
            ' Determine the IP Address of this machine
            Dim aryLocalAddr() As IPAddress = Nothing
            Dim strHostName As String = String.Empty
            Try
                ' NOTE: DNS lookups are nice and all but quite time consuming.
                strHostName = Dns.GetHostName()
                Dim ipEntry As IPHostEntry = Dns.GetHostEntry(strHostName)
                aryLocalAddr = ipEntry.AddressList
            Catch ex As Exception
                Console.WriteLine("Error trying to get local address {0} ", ex.Message)
            End Try

            ' Verify we got an IP address. Tell the user if we did
            If aryLocalAddr Is Nothing OrElse aryLocalAddr.Length < 1 Then
                Console.WriteLine("Unable to get local address")
                Return
            End If
            Console.WriteLine("Listening on : [{0}] {1}:{2}", strHostName, aryLocalAddr(0), nPortListen)

            ' Create the listener socket in this machines IP address
            Dim listener As New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
            listener.Bind(New IPEndPoint(aryLocalAddr(0), 399))
            'listener.Bind( new IPEndPoint( IPAddress.Loopback, 399 ) );	// For use with localhost 127.0.0.1
            listener.Listen(10)

            ' Setup a callback to be notified of connection requests
            listener.BeginAccept(New AsyncCallback(AddressOf app.OnConnectRequest), listener)

            Console.WriteLine("Press Enter to exit")
            Console.ReadLine()
            Console.WriteLine("OK that does it! Screw you guys I'm going home...")

            ' Clean up before we go home
            listener.Close()
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Sub


        ''' <summary>
        ''' Callback used when a client requests a connection. 
        ''' Accpet the connection, adding it to our list and setup to 
        ''' accept more connections.
        ''' </summary>
        ''' <param name="ar"></param>
        Public Sub OnConnectRequest(ByVal ar As IAsyncResult)
            Dim listener As Socket = CType(ar.AsyncState, Socket)
            NewConnection(listener.EndAccept(ar))
            listener.BeginAccept(New AsyncCallback(AddressOf OnConnectRequest), listener)
        End Sub

        ''' <summary>
        ''' Add the given connection to our list of clients
        ''' Note we have a new friend
        ''' Send a welcome to the new client
        ''' Setup a callback to recieve data
        ''' </summary>
        ''' <param name="sockClient">Connection to keep</param>
        'public void NewConnection( TcpListener listener )
        Public Sub NewConnection(ByVal sockClient As Socket)
            ' Program blocks on Accept() until a client connects.
            'SocketChatClient client = new SocketChatClient( listener.AcceptSocket() );
            Dim client As New SocketChatClient(sockClient)
            _Clients.Add(client)
            Console.WriteLine("Client {0}, joined", client.Sock.RemoteEndPoint)

            ' Get current date and time.
            Dim now As DateTimeOffset = DateTimeOffset.Now
            Dim strDateLine As String = $"Welcome {now.ToString("G")}{vbLf}{vbCr}"

            ' Convert to byte array and send.
            Dim byteDateLine() As Byte = System.Text.Encoding.ASCII.GetBytes(strDateLine.ToCharArray())
            client.Sock.Send(byteDateLine, byteDateLine.Length, 0)

            client.SetupRecieveCallback(Me)
        End Sub

        ''' <summary>
        ''' Get the new data and send it out to all other connections. 
        ''' Note: If not data was received the connection has probably 
        ''' died.
        ''' </summary>
        ''' <param name="ar"></param>
        <Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
        Public Sub OnRecievedData(ByVal ar As IAsyncResult)
            Dim client As SocketChatClient = CType(ar.AsyncState, SocketChatClient)
            Dim aryRet() As Byte = client.GetRecievedData(ar)

            ' If no data was received then the connection is probably dead
            If aryRet.Length < 1 Then
                Console.WriteLine("Client {0}, disconnected", client.Sock.RemoteEndPoint)
                client.Sock.Close()
                _Clients.Remove(client)
                Return
            End If

            ' Send the received data to all clients (including sender for echo)
            For Each clientSend As SocketChatClient In _Clients
                Try
                    clientSend.Sock.Send(aryRet)
                Catch
                    ' If the send fails the close the connection
                    Console.WriteLine("Send to client {0} failed", client.Sock.RemoteEndPoint)
                    clientSend.Sock.Close()
                    _Clients.Remove(client)
                    Return
                End Try
            Next clientSend
            client.SetupRecieveCallback(Me)
        End Sub
    End Class

	''' <summary>
	''' Class holding information and buffers for the Client socket connection
	''' </summary>
	Friend Class SocketChatClient

        Private ReadOnly _ByBuff(49) As Byte ' Receive data buffer
        ''' <summary>
        ''' Constructor
        ''' </summary>
        ''' <param name="sock">client socket connection this object represents</param>
        Public Sub New(ByVal sock As Socket)
            _Sock = sock
        End Sub

        Public ReadOnly Property Sock() As Socket

        ''' <summary>
        ''' Setup the callback for received data and loss of connection
        ''' </summary>
        ''' <param name="app"></param>
        <Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
        Public Sub SetupRecieveCallback(ByVal app As AppMain)
            Try
                Dim recieveData As New AsyncCallback(AddressOf app.OnRecievedData)
                _Sock.BeginReceive(_ByBuff, 0, _ByBuff.Length, SocketFlags.None, recieveData, Me)
            Catch ex As Exception
                Console.WriteLine("Receive callback setup failed! {0}", ex.Message)
            End Try
        End Sub

        ''' <summary>
        ''' Data has been received so we shall put it in an array and
        ''' return it.
        ''' </summary>
        ''' <param name="ar"></param>
        ''' <returns>Array of bytes containing the received data</returns>
        <Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
        Public Function GetRecievedData(ByVal ar As IAsyncResult) As Byte()
            Dim nBytesRec As Integer = 0
            Try
                nBytesRec = _Sock.EndReceive(ar)
            Catch
            End Try
            Dim byReturn(nBytesRec - 1) As Byte
            Array.Copy(_ByBuff, byReturn, nBytesRec)

            '			
            '			// Check for any remaining data and display it
            '			// This will improve performance for large packets 
            '			// but adds nothing to readability and is not essential
            '			int nToBeRead = m_sock.Available;
            '			if( nToBeRead > 0 )
            '			{
            '				byte [] byData = new byte[nToBeRead];
            '				m_sock.Receive( byData );
            '				// Append byData to byReturn here
            '			}
            '			
            Return byReturn
        End Function
    End Class
End Namespace
