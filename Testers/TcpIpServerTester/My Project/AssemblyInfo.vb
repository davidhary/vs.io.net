Imports System
Imports System.Reflection
Imports System.Runtime.CompilerServices
'
' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.
'
<Assembly: AssemblyTitle("TcpIp Server Tester")> 
<Assembly: AssemblyDescription("TcpIp Server Tester")> 
<Assembly: AssemblyProduct("TcpIP.Server.Tester")> 

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: System.Runtime.InteropServices.ComVisible(False)> 
