Imports isr.Core.EscapeSequencesExtensions
Imports isr.Net.Core.TcpIP

''' <summary> This form connects to a Socket server and Streams data to and from it. Note: The
''' following has been omitted. <para>
'''     1) Send button need to be grayed when connection is not active;</para><para>
'''     2) Send button should gray when no text in the Message box.</para><para>
'''     3) Line feeds in the received data should be parsed into separate lines in the received data list</para><para>
'''     4) Read startup setting from a app.config file. </para></summary>
''' <remarks> http://www.codeproject.com/Articles/1608/Asynchronous-socket-communication. (c) 2001 John McTainsh. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/29/2015 </para></remarks>
Public Class Form1
    Inherits System.Windows.Forms.Form

    Public Sub New()
        '
        ' Required for Windows Form Designer support
        '
        Me.InitializeComponent()

        Me._Session = New SocketSession
        Me._SendButton.Enabled = False
        Me._ServerAddressComboBox.Text = "192.168.1.107:5025"

    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnDispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Me._Session IsNot Nothing Then
                    Me._Session.Dispose()
                    Me._Session = Nothing
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

#Region " SESSION "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Session As SocketSession
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the session. </summary>
    ''' <value> The session. </value>
    Public ReadOnly Property Session As SocketSession
        Get
            Return Me._Session
        End Get
    End Property

    Private Sub Session_Connected(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Session.Connected
        Me.Invoke(New MethodInvoker(Sub() My.Settings.PortNumber = Me._PortNumeric.Value))
        Me.Invoke(New MethodInvoker(Sub() My.Settings.ServerAddress = Me._ServerAddressComboBox.Text))
        Me.Session.EnableReceiveEvents()
        Me.UpdateConnectLabel(True)
        Me.Invoke(New MethodInvoker(Sub() Me._SendButton.Enabled = True))
        Me.Invoke(New MethodInvoker(Sub() Me._ConnectCheckBox.Enabled = True))
        Windows.Forms.Application.DoEvents()
    End Sub

    Private Sub Session_ConnectionLost(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Session.ConnectionLost

        Me.UpdateConnectLabel(False)
        Me.Invoke(New MethodInvoker(Sub() Me._SendButton.Enabled = False))
        Me.Invoke(New MethodInvoker(Sub() Me._ConnectCheckBox.Enabled = True))
        Windows.Forms.Application.DoEvents()
    End Sub

    Private Sub Session_Disconnected(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Session.Disconnected
        Me.UpdateConnectLabel(False)
        Me.Invoke(New MethodInvoker(Sub() Me._SendButton.Enabled = False))

        Me.Invoke(New MethodInvoker(Sub() Me._ConnectCheckBox.Enabled = True))
        Windows.Forms.Application.DoEvents()
    End Sub

    Private ReadOnly _ReceivePrefix As String = "r: "

    Private Sub Session_Received(ByVal sender As Object, ByVal e As MessageEventArgs) Handles _Session.Received
        Dim value As String = e.Value.InsertCommonEscapeSequences
        Me.Invoke(New MethodInvoker(Sub() Me._DataListBox.Items.Add(String.Format("{0}{1}",
                                                                                  Me._ReceivePrefix, value))))
        Windows.Forms.Application.DoEvents()
    End Sub

    Private ReadOnly _SendPrefix As String = "s: "

    Private Sub Session_Sent(ByVal sender As Object, ByVal e As MessageEventArgs) Handles _Session.Sent
        Dim value As String = e.Value.InsertCommonEscapeSequences
        Me.Invoke(New MethodInvoker(Sub() Me._DataListBox.Items.Add(String.Format("{0}{1}",
                                                                                  Me._SendPrefix, value))))
        Windows.Forms.Application.DoEvents()
    End Sub

    Private Sub Session_ErrorOccurred(sender As Object, e As System.IO.ErrorEventArgs) Handles _Session.ErrorOccurred

        Dim caption As String = "Session exception"
        Dim ex As Exception = e.GetException
        If ex IsNot Nothing Then
            Dim newCaption As New System.Text.StringBuilder
            For Each item As Object In ex.Data
                If TypeOf item Is String Then
                    If newCaption.Length > 0 Then
                        newCaption.AppendFormat("; {0}", item)
                    Else
                        newCaption.Append(CStr(item))
                    End If
                End If
            Next
            If newCaption.Length > 0 Then caption = newCaption.ToString
        End If
        Me.Invoke(New MethodInvoker(Sub() System.Windows.Forms.MessageBox.Show(e.GetException.Message, caption)))
        Windows.Forms.Application.DoEvents()
    End Sub

#End Region

#Region " CONTROL EVENTS "

    Private Sub UpdateConnectLabel(ByVal isConnected As Boolean)
        Me.Invoke(New MethodInvoker(Sub() Me._ConnectCheckBox.Text = CStr(IIf(isConnected, "Dis&connect", "&Connect"))))
    End Sub

    ''' <summary> Connect button pressed. Attempt a connection to the server and setup Received data callback </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConnectButton_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ConnectCheckBox.CheckStateChanged

        Dim currentCursor As Cursor = Cursor.Current
        Try
            Cursor.Current = Cursors.WaitCursor
            Me._ConnectCheckBox.Enabled = False
            Me._SendButton.Enabled = False

            If Me._ConnectCheckBox.Checked AndAlso Not Me.Session.IsConnected Then
                Me.Session.BeginConnect(Me._ServerAddressComboBox.Text)
            ElseIf Not Me._ConnectCheckBox.Checked AndAlso Me.Session.IsConnected Then
                Me.Session.BeginDisconnect()
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message, "Server Disconnect/Connect failed!")
            System.Windows.Forms.MessageBox.Show(ex.ToString, "Server failure details")
        Finally
            Me._ConnectCheckBox.Enabled = True
            Cursor.Current = currentCursor
        End Try

    End Sub

    ''' <summary> Send the Message in the Message area. Only do this if we are connected </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SendButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SendButton.Click
        Try
            ' replace the escape characters with standard values.
            Dim value As String = Me._MessageTextBox.Text.ReplaceCommonEscapeSequences
            Me.Session.Send(value)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message, "Send Message Failed!")
        End Try
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FindButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FindButton.Click
        Dim curs As Cursor = Me.Cursor
        Try
            Me.Cursor = Cursors.WaitCursor
            ' Dim port As Integer = CInt(Me._PortNumeric.Value) '  SessionBase.ParsePort(Me._ServerAddressComboBox.Text)
            Me._ServerAddressComboBox.DataSource = ClientSession.Discover(CInt(Me._PortNumeric.Value),
                                                                          TimeSpan.FromMilliseconds(Me._TimeoutNumeric.Value))
            Me._ServerAddressComboBox.Refresh()
            Windows.Forms.Application.DoEvents()
        Catch ex As Exception
            Me._ServerAddressComboBox.DataSource = New String() {"Failed"}
        Finally
            Me.Cursor = curs
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Close the Socket connection before going home </summary>
    Private Sub Form1_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If Me._Session IsNot Nothing Then
            Me._Session.Disconnect()
        End If
    End Sub

    Private Sub Form1_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Me._PortNumeric.Value = My.Settings.PortNumber
        Me._ServerAddressComboBox.Text = My.Settings.ServerAddress
    End Sub


#End Region

End Class

