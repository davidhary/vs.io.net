﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class Form1

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                Me.onDispose(disposing)
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me._SendButton = New System.Windows.Forms.Button()
        Me._MessageTextBox = New System.Windows.Forms.TextBox()
        Me._DataListBox = New System.Windows.Forms.ListBox()
        Me._ConnectCheckBox = New System.Windows.Forms.CheckBox()
        Me._ServerAddressComboBox = New System.Windows.Forms.ComboBox()
        Me._FindButton = New System.Windows.Forms.Button()
        Me._TimeoutNumeric = New System.Windows.Forms.NumericUpDown()
        Me._TimeoutNumericUnitsLabel = New System.Windows.Forms.Label()
        Me._PortNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me._TimeoutNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._PortNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_SendButton
        '
        Me._SendButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SendButton.Location = New System.Drawing.Point(229, 55)
        Me._SendButton.Name = "_SendButton"
        Me._SendButton.Size = New System.Drawing.Size(75, 23)
        Me._SendButton.TabIndex = 7
        Me._SendButton.Text = "Send"
        '
        '_MessageTextBox
        '
        Me._MessageTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._MessageTextBox.Location = New System.Drawing.Point(9, 56)
        Me._MessageTextBox.Name = "_MessageTextBox"
        Me._MessageTextBox.Size = New System.Drawing.Size(213, 20)
        Me._MessageTextBox.TabIndex = 6
        Me._MessageTextBox.Text = "*IDN?\n"
        '
        '_DataListBox
        '
        Me._DataListBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._DataListBox.IntegralHeight = False
        Me._DataListBox.Location = New System.Drawing.Point(1, 89)
        Me._DataListBox.Name = "_DataListBox"
        Me._DataListBox.Size = New System.Drawing.Size(311, 214)
        Me._DataListBox.TabIndex = 8
        '
        '_ConnectCheckBox
        '
        Me._ConnectCheckBox.Appearance = System.Windows.Forms.Appearance.Button
        Me._ConnectCheckBox.Location = New System.Drawing.Point(148, 28)
        Me._ConnectCheckBox.Name = "_ConnectCheckBox"
        Me._ConnectCheckBox.Size = New System.Drawing.Size(75, 23)
        Me._ConnectCheckBox.TabIndex = 5
        Me._ConnectCheckBox.Text = "&Connect"
        Me._ConnectCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._ConnectCheckBox.UseVisualStyleBackColor = True
        '
        '_ServerAddressComboBox
        '
        Me._ServerAddressComboBox.FormattingEnabled = True
        Me._ServerAddressComboBox.Location = New System.Drawing.Point(9, 28)
        Me._ServerAddressComboBox.Name = "_ServerAddressComboBox"
        Me._ServerAddressComboBox.Size = New System.Drawing.Size(133, 21)
        Me._ServerAddressComboBox.TabIndex = 4
        Me._ServerAddressComboBox.Text = "192.168.1.112:5025"
        '
        '_FindButton
        '
        Me._FindButton.Location = New System.Drawing.Point(8, 1)
        Me._FindButton.Name = "_FindButton"
        Me._FindButton.Size = New System.Drawing.Size(69, 23)
        Me._FindButton.TabIndex = 0
        Me._FindButton.Text = "&Find Port"
        Me._FindButton.UseVisualStyleBackColor = True
        '
        '_TimeoutNumeric
        '
        Me._TimeoutNumeric.Location = New System.Drawing.Point(148, 2)
        Me._TimeoutNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me._TimeoutNumeric.Name = "_TimeoutNumeric"
        Me._TimeoutNumeric.Size = New System.Drawing.Size(41, 20)
        Me._TimeoutNumeric.TabIndex = 2
        Me._ToolTip.SetToolTip(Me._TimeoutNumeric, "Ping timeout")
        Me._TimeoutNumeric.Value = New Decimal(New Integer() {20, 0, 0, 0})
        '
        '_TimeoutNumericUnitsLabel
        '
        Me._TimeoutNumericUnitsLabel.AutoSize = True
        Me._TimeoutNumericUnitsLabel.Location = New System.Drawing.Point(191, 6)
        Me._TimeoutNumericUnitsLabel.Name = "_TimeoutNumericUnitsLabel"
        Me._TimeoutNumericUnitsLabel.Size = New System.Drawing.Size(20, 13)
        Me._TimeoutNumericUnitsLabel.TabIndex = 3
        Me._TimeoutNumericUnitsLabel.Text = "ms"
        '
        '_PortNumeric
        '
        Me._PortNumeric.Location = New System.Drawing.Point(80, 2)
        Me._PortNumeric.Maximum = New Decimal(New Integer() {32767, 0, 0, 0})
        Me._PortNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me._PortNumeric.Name = "_PortNumeric"
        Me._PortNumeric.Size = New System.Drawing.Size(62, 20)
        Me._PortNumeric.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._PortNumeric, "Port number")
        Me._PortNumeric.Value = New Decimal(New Integer() {5025, 0, 0, 0})
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(312, 308)
        Me.Controls.Add(Me._PortNumeric)
        Me.Controls.Add(Me._TimeoutNumericUnitsLabel)
        Me.Controls.Add(Me._TimeoutNumeric)
        Me.Controls.Add(Me._FindButton)
        Me.Controls.Add(Me._ServerAddressComboBox)
        Me.Controls.Add(Me._ConnectCheckBox)
        Me.Controls.Add(Me._SendButton)
        Me.Controls.Add(Me._MessageTextBox)
        Me.Controls.Add(Me._DataListBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TCP/IP Client"
        CType(Me._TimeoutNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._PortNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SendButton As System.Windows.Forms.Button
    Private WithEvents _MessageTextBox As System.Windows.Forms.TextBox
    Private WithEvents _DataListBox As System.Windows.Forms.ListBox
    Private WithEvents _ConnectCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ServerAddressComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _FindButton As System.Windows.Forms.Button
    Private WithEvents _TimeoutNumeric As NumericUpDown
    Private WithEvents _ToolTip As ToolTip
    Private WithEvents _TimeoutNumericUnitsLabel As Label
    Private WithEvents _PortNumeric As NumericUpDown
End Class
