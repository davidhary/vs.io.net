﻿Imports System
Imports System.Reflection
'
' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.
'
<Assembly: AssemblyTitle("TCP/IP Session Tester")> 
<Assembly: AssemblyDescription("TCP/IP Session Tester")> 
<Assembly: AssemblyProduct("TcpIP.Session.Tester")> 
#If Not NonClsCompliant Then
<Assembly: CLSCompliant(True)> 
#End If

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: System.Runtime.InteropServices.ComVisible(False)> 

