﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                Me.onDispose(disposing)
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._SendButton = New System.Windows.Forms.Button()
        Me._MessageTextBox = New System.Windows.Forms.TextBox()
        Me._ReceivedDataListBox = New System.Windows.Forms.ListBox()
        Me._ServerAddressTextBox = New System.Windows.Forms.TextBox()
        Me._ConnectButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        '_SendButton
        '
        Me._SendButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SendButton.Location = New System.Drawing.Point(229, 32)
        Me._SendButton.Name = "_SendButton"
        Me._SendButton.Size = New System.Drawing.Size(75, 23)
        Me._SendButton.TabIndex = 9
        Me._SendButton.Text = "Send"
        '
        '_MessageTextBox
        '
        Me._MessageTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._MessageTextBox.Location = New System.Drawing.Point(9, 33)
        Me._MessageTextBox.Name = "_MessageTextBox"
        Me._MessageTextBox.Size = New System.Drawing.Size(205, 20)
        Me._MessageTextBox.Text = "*IDN?\n"
        Me._MessageTextBox.TabIndex = 8
        '
        '_ReceivedDataListBox
        '
        Me._ReceivedDataListBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ReceivedDataListBox.IntegralHeight = False
        Me._ReceivedDataListBox.Location = New System.Drawing.Point(1, 62)
        Me._ReceivedDataListBox.Name = "_ReceivedDataListBox"
        Me._ReceivedDataListBox.Size = New System.Drawing.Size(311, 220)
        Me._ReceivedDataListBox.TabIndex = 7
        '
        '_ServerAddressTextBox
        '
        Me._ServerAddressTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ServerAddressTextBox.Location = New System.Drawing.Point(9, 4)
        Me._ServerAddressTextBox.Name = "_ServerAddressTextBox"
        Me._ServerAddressTextBox.Size = New System.Drawing.Size(204, 20)
        Me._ServerAddressTextBox.TabIndex = 6
        Me._ServerAddressTextBox.Text = "192.168.1.106:5025"
        '
        '_ConnectButton
        '
        Me._ConnectButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ConnectButton.Location = New System.Drawing.Point(229, 4)
        Me._ConnectButton.Name = "_ConnectButton"
        Me._ConnectButton.Size = New System.Drawing.Size(75, 23)
        Me._ConnectButton.TabIndex = 5
        Me._ConnectButton.Text = "Connect"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(312, 287)
        Me.Controls.Add(Me._SendButton)
        Me.Controls.Add(Me._MessageTextBox)
        Me.Controls.Add(Me._ReceivedDataListBox)
        Me.Controls.Add(Me._ServerAddressTextBox)
        Me.Controls.Add(Me._ConnectButton)
        Me.Name = "Form1"
        Me.Text = "TcpIp Client"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SendButton As System.Windows.Forms.Button
    Private WithEvents _MessageTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ReceivedDataListBox As System.Windows.Forms.ListBox
    Private WithEvents _ServerAddressTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ConnectButton As System.Windows.Forms.Button
End Class
