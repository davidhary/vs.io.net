Imports System.Text
Imports System.Net.Sockets
Imports System.Net
Imports isr.Core.EscapeSequencesExtensions

''' <summary> This form connects to a Socket server and Streams data to and from it. Note: The
''' following has been omitted. <para>
'''     1) Send button need to be grayed when connection is not active;</para><para>
'''     2) Send button should gray when no text in the Message box.</para><para>
'''     3) Line feeds in the received data should be parsed into separate lines in the received data list</para><para>
'''     4) Read startup setting from a app.config file. </para></summary>
''' <remarks> http://www.codeproject.com/Articles/1608/Asynchronous-socket-communication. (c) 2001 John McTainsh. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/29/2015 </para></remarks>
Public Class Form1

    Public Sub New()
        '
        ' Required for Windows Form Designer support
        '
        Me.InitializeComponent()

        '
        ' TODO: Add any constructor code after InitializeComponent call
        Me._SendButton.Enabled = False

    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnDispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me._Socket IsNot Nothing Then
                If Me._Socket.Connected Then
                    Me._Socket.Shutdown(SocketShutdown.Both)
                    System.Threading.Thread.Sleep(10)
                    Me._Socket.Close()
                End If
                Me._Socket.Dispose()
                Me._Socket = Nothing
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

    Private _Socket As Socket ' Server connection
    Private ReadOnly _ReceiveBuffer(255) As Byte ' Received data buffer

    ''' <summary> Connect button pressed. Attempt a connection to the server and setup Received data callback </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConnectButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ConnectButton.Click

        Dim currentCursor As Cursor = Cursor.Current
        Try

            Cursor.Current = Cursors.WaitCursor

            ' Close the socket if it is still open
            If Me._Socket IsNot Nothing AndAlso Me._Socket.Connected Then
                Me._Socket.Shutdown(SocketShutdown.Both)
                System.Threading.Thread.Sleep(10)

                Me._Socket.Close()
            End If

            ' Create the socket object
            Me._Socket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)

            Dim port As Integer = 5025
            Dim address As String() = Me._ServerAddressTextBox.Text.Split(":"c)
            Dim ip As String = address(0)
            If address.Length > 1 Then Integer.TryParse(address(1), port)

            ' Define the Server address and port
            Dim serverEndPoint As New IPEndPoint(IPAddress.Parse(ip), port)

            ' Connect to the server blocking method and setup callback for received data
            ' m_sock.Connect( epServer );
            ' SetupRecieveCallback( m_sock );

            ' Connect to server non-Blocking method
            Me._Socket.Blocking = False
            Me._Socket.BeginConnect(serverEndPoint, New AsyncCallback(AddressOf Me.OnConnect), Me._Socket)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message, "Server Connect failed!")
        Finally
            Cursor.Current = currentCursor
        End Try

    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub OnConnect(ByVal asyncResult As IAsyncResult)

        ' Check if we were successful
        Try
            ' Socket was the passed in object
            Dim sock As Socket = CType(asyncResult.AsyncState, Socket)
            If sock.Connected Then
                Me.SetupRecieveCallback(sock)
            Else
                Me.Invoke(New MethodInvoker(Sub() Me._SendButton.Enabled = False))
                Me.Invoke(New MethodInvoker(Sub() System.Windows.Forms.MessageBox.Show("Unable to connect to remote machine", "Connect Failed!")))
            End If
        Catch ex As Exception
            Me.Invoke(New MethodInvoker(Sub() System.Windows.Forms.MessageBox.Show(ex.Message, "Unusual error during Connect!")))
        End Try
    End Sub

    ''' <summary> Get the new data and send it out to all other connections. If not data was received
    ''' the connection has probably died. </summary>
    ''' <param name="ar"> . </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub OnRecievedData(ByVal asyncResult As IAsyncResult)

        ' Socket was the passed in object
        Dim sock As Socket = CType(asyncResult.AsyncState, Socket)

        ' Check if we got any data
        Try

            Dim receivedMessageLength As Integer = sock.EndReceive(asyncResult)
            If receivedMessageLength > 0 Then

                ' Wrote the data to the List
                Dim receivedString As String = Encoding.ASCII.GetString(Me._ReceiveBuffer, 0, receivedMessageLength)

                ' insert escape characters
                receivedString = receivedString.InsertCommonEscapeSequences

                ' Invoke(m_AddMessageEvent, New String() {sReceived})
                Me.Invoke(New MethodInvoker(Sub() Me._ReceivedDataListBox.Items.Add(receivedString)))

                ' If the connection is still usable reestablish the callback
                Me.SetupRecieveCallback(sock)
            Else
                ' If no data was received then the connection is probably dead
                Console.WriteLine("Client {0} disconnected", sock.RemoteEndPoint)
                sock.Shutdown(SocketShutdown.Both)
                sock.Close()
            End If
        Catch ex As Exception
            Me.Invoke(New MethodInvoker(Sub() System.Windows.Forms.MessageBox.Show(ex.Message, "Unusual error during Receive!")))
        End Try
    End Sub

    ''' <summary> Setup the callback for received data and loss of connection </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub SetupRecieveCallback(ByVal sock As Socket)
        Try
            Me.Invoke(New MethodInvoker(Sub() Me._SendButton.Enabled = True))
            sock.BeginReceive(Me._ReceiveBuffer, 0, Me._ReceiveBuffer.Length, SocketFlags.None,
                              New AsyncCallback(AddressOf Me.OnRecievedData), sock)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message, "Setup Receive Callback failed!")
        End Try
    End Sub

    ''' <summary> Close the Socket connection before going home </summary>
    Private Sub FormMain_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If Me._Socket IsNot Nothing AndAlso Me._Socket.Connected Then
            Me._Socket.Shutdown(SocketShutdown.Both)
            Me._Socket.Close()
        End If
    End Sub

    ''' <summary> Send the Message in the Message area. Only do this if we are connected </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SendButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SendButton.Click
        ' Check we are connected
        If Me._Socket Is Nothing OrElse (Not Me._Socket.Connected) Then
            System.Windows.Forms.MessageBox.Show("Must be connected to Send a message")
            Return
        End If

        ' Read the message from the text box and send it
        Try
            ' replace the escape characters with standard values.
            Dim value As String = Me._MessageTextBox.Text.ReplaceCommonEscapeSequences
            ' Convert to byte array and send.
            Dim byteDateLine() As Byte = Encoding.ASCII.GetBytes(value.ToCharArray())
#If False Then
            Dim newArray(byteDateLine.Length) As Byte
            byteDateLine.CopyTo(newArray, 0)
            newArray(byteDateLine.Length) = 10
            byteDateLine = newArray
#End If
            Me._Socket.Send(byteDateLine, byteDateLine.Length, 0)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message, "Send Message Failed!")
        End Try
    End Sub

End Class

