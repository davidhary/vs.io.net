﻿Imports System.Net
Imports isr.Net.Core.Udp

''' <summary> This form connects to a UDP server and Streams data to and from it. </summary>
''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 11/16/2015 </para></remarks>
Friend Class Form1

    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnDispose(ByVal disposing As Boolean)
        If disposing Then
            Try
                If client IsNot Nothing Then
                    client.Dispose()
                    client = Nothing
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        End If
    End Sub

    Private WithEvents Client As Client

    Private Property PortNumber As Integer = 5

    Private Property Timeout As Byte = 5

    ''' <summary> Event handler. Called by SendButton for click events. </summary>
    ''' <remarks> The event above is the implementation of a button click event. In here, I call the
    ''' client.QueueMessage(); method. This method is so called as messages are sent asynchronously
    ''' and thus queued until they are ready to be sent. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SendButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _SendButton.Click
        If client Is Nothing Then
            Me._ActivityListBox.Items.Add("no client")
        Else
            'Dim value As String = IPAddress.Parse(_IpTextBox.Text).ToString
            Me._ActivityListBox.Items.Add("Attempt Send message: " &
                                      client.QueueMessage(System.Text.Encoding.ASCII.GetBytes(Me._ToSendTextBox.Text & vbCr), 64,
                                                          New System.Net.IPEndPoint(IPAddress.Parse(_IpTextBox.Text), Me.PortNumber),
                                                          TimeSpan.FromSeconds(Me.Timeout),
                                                          Me._ReliableCheckBox.Checked).ToString())
        End If
    End Sub

    ''' <summary> Form 1 load. </summary>
    ''' <remarks> In the snippet above, the Windows Form's Load event is used to instantiate the Client
    ''' class. For this particular overload, the local host I.P. address is passed as the local end
    ''' point. The second parameter is the packet size. This number is the number of bytes into which
    ''' messages will be broken up into if the sent message is larger than the number. For instance,
    ''' if the message you send is 3092 bytes long, the message will be sent as three packets of 1024
    ''' bytes and a fourth packet of 20 bytes. The third parameter is the time in seconds that the
    ''' client will wait after sending a packet, before assuming the packet has failed and resending
    ''' it. The default number of times a packet will be attempted to send is three. This value can
    ''' be overridden using a different overload for the constructor. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form1_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'client = New Client(New IPEndPoint(IPAddress.Any, 5), 1024, 10)
        Me._IpTextBox.Text = "127.000.000.001"
        ' Me._IpTextBox.Text = "192.168.001.107"
        ' client = New Client(New IPEndPoint(IPAddress.Parse("127.0.0.1"), 5), 1024, 10)
    End Sub

    ''' <summary> Client message received. </summary>
    ''' <remarks> The two events of the Client class are bound to methods here. OnMessageSent is bound
    ''' to client_MessageSent(); and OnMessageReceived is bound to OnMessageReceived();. These events
    ''' are raised after a message has been sent and received respectively.
    ''' <para>In the implementation of the event method above, I simply add the received message's
    ''' text to a text
    ''' box on the Windows Form.</para>
    ''' <para> Of course, this wouldn't make sense if for instance a binary file was sent as the
    ''' message. The <see cref="isr.Net.Core.Udp.MessageReceivedEventArgs">arguments</see>  passed to the method
    ''' contains the data that was received as a byte array.</para> </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="args">   Message received event information. </param>
    Private Sub Client_MessageReceived(sender As Object, args As MessageReceivedEventArgs) Handles client.MessageReceived
        Dim prefix As String = String.Empty
        If Me._ReceivedMessageTextBox.Text.Length > 0 Then prefix = Environment.NewLine
        Dim value As String = String.Format("{0}{1}{2}",
                                            Me._ReceivedMessageTextBox.Text, prefix, System.Text.Encoding.ASCII.GetString(args.Data))
        Me.Invoke(New MethodInvoker(Sub() Me._ReceivedMessageTextBox.Text = value))
    End Sub

    ''' <summary> Client message sent. </summary>
    ''' <remarks> <para>When a message was sent not specifying the reliable flag, this event will be
    ''' raised immediately after sending the message. The <see cref="isr.Net.Core.Udp.MessageSentEventArgs">arguments</see>  contains a field
    ''' called SendStatus. This field will be set to Sent indicating the message was sent, but no
    ''' further assumption can be made as to reaching its destination.
    ''' </para><para>
    ''' If the message was sent reliably, that is, sending with the Reliable flag set to true, the
    ''' <see cref="isr.Net.Core.Udp.MessageSentEventArgs">arguments</see> will have either the value Delivered or Failed in the SendStatus field.
    ''' When the value is Delivered, the consumer can then safely assume the entire message was
    ''' received by the recipient. Conversely, when the value is Failed, the message was sent, but no
    ''' delivery confirmation was received. The consumer should in this case assume the message
    ''' failed.
    ''' </para> </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="args">   Message sent event information. </param>
    Private Sub Client_MessageSent(sender As Object, args As MessageSentEventArgs) Handles client.MessageSent
        Select Case args.Status
            Case SendStatus.Failed
                Me.Invoke(New MethodInvoker(Sub() Me._ActivityListBox.Items.Add("Failed message: " & args.MessageId.ToString())))

            Case SendStatus.Sent
                Me.Invoke(New MethodInvoker(Sub() Me._ActivityListBox.Items.Add("Sent message: " & args.MessageId.ToString())))

            Case SendStatus.Delivered
                Me.Invoke(New MethodInvoker(Sub() Me._ActivityListBox.Items.Add("Delivered message: " & args.MessageId.ToString())))
        End Select
    End Sub

    Private Sub InitializeButton_Click(sender As System.Object, e As System.EventArgs) Handles _InitializeButton.Click
        client = New Client(New IPEndPoint(IPAddress.Loopback, Me.PortNumber), 1024, 10)
        _ReceiveAddressLabel.Text = client.LocalEndPoint.Address.ToString
    End Sub

    Private Sub PortNumberNumeric_ValueChanged(sender As System.Object, e As System.EventArgs) Handles _PortNumberNumeric.ValueChanged
        Me.PortNumber = CInt(Me._PortNumberNumeric.Value)
    End Sub

    Private Sub TimeoutNumeric_ValueChanged(sender As System.Object, e As System.EventArgs) Handles _TimeoutNumeric.ValueChanged
        Me.Timeout = CByte(Me._TimeoutNumeric.Value)
    End Sub

End Class

