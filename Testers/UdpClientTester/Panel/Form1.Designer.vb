﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                Me.onDispose(disposing)
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._SendButton = New System.Windows.Forms.Button()
        Me._ActivityListBox = New System.Windows.Forms.ListBox()
        Me._IpTextBox = New System.Windows.Forms.MaskedTextBox()
        Me._ToSendTextBox = New System.Windows.Forms.TextBox()
        Me._PortNumberNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ReliableCheckBox = New System.Windows.Forms.CheckBox()
        Me._InitializeButton = New System.Windows.Forms.Button()
        Me._ReceivedMessageTextBox = New System.Windows.Forms.TextBox()
        Me._ReceiveAddressLabel = New System.Windows.Forms.Label()
        Me._IpTextBoxLabel = New System.Windows.Forms.Label()
        Me._PortNumberNumericLabel = New System.Windows.Forms.Label()
        Me._TimeoutNumericLabel = New System.Windows.Forms.Label()
        Me._TimeoutNumeric = New System.Windows.Forms.NumericUpDown()
        CType(Me._PortNumberNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._TimeoutNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_SendButton
        '
        Me._SendButton.Location = New System.Drawing.Point(11, 23)
        Me._SendButton.Name = "_SendButton"
        Me._SendButton.Size = New System.Drawing.Size(55, 23)
        Me._SendButton.TabIndex = 1
        Me._SendButton.Text = "Send"
        Me._SendButton.UseVisualStyleBackColor = True
        '
        '_ActivityListBox
        '
        Me._ActivityListBox.FormattingEnabled = True
        Me._ActivityListBox.Location = New System.Drawing.Point(11, 52)
        Me._ActivityListBox.Name = "_ActivityListBox"
        Me._ActivityListBox.Size = New System.Drawing.Size(447, 108)
        Me._ActivityListBox.TabIndex = 2
        '
        '_IpTextBox
        '
        Me._IpTextBox.Location = New System.Drawing.Point(89, 1)
        Me._IpTextBox.Mask = "###.###.###.###"
        Me._IpTextBox.Name = "_IpTextBox"
        Me._IpTextBox.Size = New System.Drawing.Size(100, 20)
        Me._IpTextBox.TabIndex = 3
        Me._IpTextBox.Text = "192168001102"
        '
        '_ToSendTextBox
        '
        Me._ToSendTextBox.Location = New System.Drawing.Point(70, 23)
        Me._ToSendTextBox.Multiline = True
        Me._ToSendTextBox.Name = "_ToSendTextBox"
        Me._ToSendTextBox.Size = New System.Drawing.Size(390, 23)
        Me._ToSendTextBox.TabIndex = 4
        '
        '_PortNumberNumeric
        '
        Me._PortNumberNumeric.Location = New System.Drawing.Point(224, 1)
        Me._PortNumberNumeric.Maximum = New Decimal(New Integer() {32000, 0, 0, 0})
        Me._PortNumberNumeric.Minimum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._PortNumberNumeric.Name = "_PortNumberNumeric"
        Me._PortNumberNumeric.Size = New System.Drawing.Size(58, 20)
        Me._PortNumberNumeric.TabIndex = 5
        Me._PortNumberNumeric.Value = New Decimal(New Integer() {5025, 0, 0, 0})
        '
        '_ReliableCheckBox
        '
        Me._ReliableCheckBox.AutoSize = True
        Me._ReliableCheckBox.Location = New System.Drawing.Point(402, 3)
        Me._ReliableCheckBox.Name = "_ReliableCheckBox"
        Me._ReliableCheckBox.Size = New System.Drawing.Size(64, 17)
        Me._ReliableCheckBox.TabIndex = 6
        Me._ReliableCheckBox.Text = "Reliable"
        Me._ReliableCheckBox.UseVisualStyleBackColor = True
        '
        '_InitializeButton
        '
        Me._InitializeButton.Location = New System.Drawing.Point(11, 0)
        Me._InitializeButton.Name = "_InitializeButton"
        Me._InitializeButton.Size = New System.Drawing.Size(55, 23)
        Me._InitializeButton.TabIndex = 7
        Me._InitializeButton.Text = "Initialize"
        Me._InitializeButton.UseVisualStyleBackColor = True
        '
        '_ReceivedMessageTextBox
        '
        Me._ReceivedMessageTextBox.Location = New System.Drawing.Point(11, 188)
        Me._ReceivedMessageTextBox.Multiline = True
        Me._ReceivedMessageTextBox.Name = "_ReceivedMessageTextBox"
        Me._ReceivedMessageTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._ReceivedMessageTextBox.Size = New System.Drawing.Size(446, 150)
        Me._ReceivedMessageTextBox.TabIndex = 8
        '
        '_ReceiveAddressLabel
        '
        Me._ReceiveAddressLabel.AutoSize = True
        Me._ReceiveAddressLabel.Location = New System.Drawing.Point(11, 172)
        Me._ReceiveAddressLabel.Name = "_ReceiveAddressLabel"
        Me._ReceiveAddressLabel.Size = New System.Drawing.Size(52, 13)
        Me._ReceiveAddressLabel.TabIndex = 9
        Me._ReceiveAddressLabel.Text = "receipt IP"
        '
        '_IpTextBoxLabel
        '
        Me._IpTextBoxLabel.AutoSize = True
        Me._IpTextBoxLabel.Location = New System.Drawing.Point(67, 5)
        Me._IpTextBoxLabel.Name = "_IpTextBoxLabel"
        Me._IpTextBoxLabel.Size = New System.Drawing.Size(20, 13)
        Me._IpTextBoxLabel.TabIndex = 10
        Me._IpTextBoxLabel.Text = "IP:"
        '
        '_PortNumberNumericLabel
        '
        Me._PortNumberNumericLabel.AutoSize = True
        Me._PortNumberNumericLabel.Location = New System.Drawing.Point(195, 5)
        Me._PortNumberNumericLabel.Name = "_PortNumberNumericLabel"
        Me._PortNumberNumericLabel.Size = New System.Drawing.Size(29, 13)
        Me._PortNumberNumericLabel.TabIndex = 11
        Me._PortNumberNumericLabel.Text = "Port:"
        '
        '_TimeoutNumericLabel
        '
        Me._TimeoutNumericLabel.AutoSize = True
        Me._TimeoutNumericLabel.Location = New System.Drawing.Point(287, 5)
        Me._TimeoutNumericLabel.Name = "_TimeoutNumericLabel"
        Me._TimeoutNumericLabel.Size = New System.Drawing.Size(62, 13)
        Me._TimeoutNumericLabel.TabIndex = 12
        Me._TimeoutNumericLabel.Text = "Timeout [s]:"
        '
        '_TimeoutNumeric
        '
        Me._TimeoutNumeric.Location = New System.Drawing.Point(352, 1)
        Me._TimeoutNumeric.Name = "_TimeoutNumeric"
        Me._TimeoutNumeric.Size = New System.Drawing.Size(41, 20)
        Me._TimeoutNumeric.TabIndex = 13
        Me._TimeoutNumeric.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(466, 350)
        Me.Controls.Add(Me._TimeoutNumeric)
        Me.Controls.Add(Me._TimeoutNumericLabel)
        Me.Controls.Add(Me._PortNumberNumericLabel)
        Me.Controls.Add(Me._IpTextBoxLabel)
        Me.Controls.Add(Me._ReceiveAddressLabel)
        Me.Controls.Add(Me._ReceivedMessageTextBox)
        Me.Controls.Add(Me._InitializeButton)
        Me.Controls.Add(Me._ReliableCheckBox)
        Me.Controls.Add(Me._PortNumberNumeric)
        Me.Controls.Add(Me._ToSendTextBox)
        Me.Controls.Add(Me._IpTextBox)
        Me.Controls.Add(Me._ActivityListBox)
        Me.Controls.Add(Me._SendButton)
        Me.Name = "Form1"
        Me.Text = "UDP Tester"
        CType(Me._PortNumberNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._TimeoutNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SendButton As System.Windows.Forms.Button
    Private WithEvents _ActivityListBox As System.Windows.Forms.ListBox
    Private WithEvents _IpTextBox As System.Windows.Forms.MaskedTextBox
    Private WithEvents _ToSendTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ReliableCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _InitializeButton As System.Windows.Forms.Button
    Private WithEvents _ReceivedMessageTextBox As System.Windows.Forms.TextBox
    Private WithEvents _IpTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PortNumberNumericLabel As System.Windows.Forms.Label
    Private WithEvents _TimeoutNumericLabel As System.Windows.Forms.Label
    Private WithEvents _TimeoutNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _PortNumberNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _ReceiveAddressLabel As System.Windows.Forms.Label

End Class
